//
//  AppDelegate.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/22/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"
#import "AboutViewController.h"
#import "AlarmsViewController.h"
#import "FavsViewController.h"
#import "SettingViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    UINavigationController* homeNavigation=[[UINavigationController alloc]initWithRootViewController:[ViewController new]];
    [homeNavigation setNavigationBarHidden:YES];
    
    UINavigationController* favNavigation=[[UINavigationController alloc]initWithRootViewController:[FavsViewController new]];
    [favNavigation setNavigationBarHidden:YES];
    
    tabBar=[[ARTabBarController alloc]
            initWithTabImageNamesNormal:DEVICE_IS_PHONE?
            [NSArray arrayWithObjects: @"btn_footer_about_s0",@"btn_footer_alarm_s0",@"btn_footer_fav_s0",@"btn_footer_settings_s0",nil]:
            [NSArray arrayWithObjects: @"iPad_btn_footer_about_s0",@"iPad_btn_footer_alarm_s0",@"iPad_btn_footer_fav_s0",@"iPad_btn_footer_settings_s0",nil]
            selected:DEVICE_IS_PHONE?
            [NSArray arrayWithObjects:@"btn_footer_about_s1",@"btn_footer_alarm_s1",@"btn_footer_fav_s1",@"btn_footer_settings_s1",nil]:
            [NSArray arrayWithObjects:@"iPad_btn_footer_about_s1",@"iPad_btn_footer_alarm_s1",@"iPad_btn_footer_fav_s1",@"iPad_btn_footer_settings_s1",nil]
            ViewControllers:[NSArray arrayWithObjects:[AboutViewController new],[AlarmsViewController new],favNavigation,[SettingViewController new],homeNavigation,nil]
            andCustomeTabsCount:4
            ];
    
    [self.window setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:DEVICE_IS_PHONE? DEVICE_IS_IPHONE5? @"main_bg":@"main_bg": @"main_bg"]]];
    [tabBar setSelectedIndex:4];
    self.window.rootViewController = tabBar;// addSubview:tabBar.view];// 
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    NSString* x=notification.alertBody;
    UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"أدعية شاملة" message:x delegate:nil cancelButtonTitle:@"تم" otherButtonTitles: nil];
    [alert show];
}

@end
