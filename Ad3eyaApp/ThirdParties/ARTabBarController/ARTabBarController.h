//
//  ARTabBarController.h
//  M7lat
//
//  Created by North coast on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARTabBarController : UITabBarController

@property (nonatomic ,strong) NSArray *tabs;
@property (nonatomic ,unsafe_unretained) UIViewController *rightViewController;

-(id)initWithTabImageNamesNormal:(NSArray*)tabImageNamesNormal 
                        selected:(NSArray*)tabImageNamesSelected 
              ViewControllers:(NSArray*)viewControllers
             andCustomeTabsCount:(int)tabsCount  ;

-(IBAction)tabTouched:(UIButton*)sender;


@end
