//
//  ARTabBarController.m
//  M7lat
//
//  Created by North coast on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#define WINDOW_DIMENSIONS [[[[UIApplication sharedApplication] windows] objectAtIndex:0] bounds]
#define DEVICE_IS_PHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define DEVICE_IS_PAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)


#import "ARTabBarController.h"

@interface ARTabBarController ()
{
    
}

@end

@implementation ARTabBarController

@synthesize tabs = _tabs;
@synthesize rightViewController = _rightViewController;


#pragma mark - extra functions

/* in case :-
 tabImageNamesNormal.cout != tabImageNamesSelected.count
 selected image will be the same as normal image.
 in case no images on tabImageNamesNormal no images are set
*/
-(id)initWithTabImageNamesNormal:(NSArray*)tabImageNamesNormal 
                        selected:(NSArray*)tabImageNamesSelected 
              ViewControllers:(NSArray*)viewControllers
             andCustomeTabsCount:(int)customeTabsCount
{
    self = [super init];
    if (self)
    {
        int tabsCount= customeTabsCount?customeTabsCount:  viewControllers.count;
        float tabWidth;
        float tabHeight;
        if (DEVICE_IS_PHONE) {
            tabHeight=40.0f;
        }else {
            tabHeight=70.0f;
        }
        NSMutableArray *temp;
        
        if (tabsCount != 0)
        {
            tabWidth =  WINDOW_DIMENSIONS.size.width / tabsCount;
            temp = [[NSMutableArray alloc] initWithCapacity:5];
        }

        UIView * bgView=[[UIView alloc]initWithFrame:CGRectMake( 0, WINDOW_DIMENSIONS.size.height - (tabHeight+9),  WINDOW_DIMENSIONS.size.width,(tabHeight+9))];
        
        //[bgView setBackgroundColor:[UIColor greenColor]];
        
        bgView.autoresizingMask= (UIViewAutoresizingFlexibleWidth |
                                  UIViewAutoresizingFlexibleTopMargin |
                                  UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin);
        [bgView setTag:100];
        [bgView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:DEVICE_IS_PHONE? @"footer_bg_lndscp":@"iPad_footer_bg"]]];
        //[self.view setBackgroundColor:[UIColor redColor]];
        self.view.autoresizingMask=(UIViewAutoresizingFlexibleWidth |
                                    UIViewAutoresizingFlexibleHeight |
                                    UIViewAutoresizingFlexibleTopMargin |
                                    UIViewAutoresizingFlexibleBottomMargin|
                                    UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin);
        [self.view addSubview:bgView];
        
        for (int i = 0; i < tabsCount; i++) {
            
                // M7lat customizations
            UIButton *button = [[UIButton alloc] initWithFrame:
                                CGRectMake( i * tabWidth,
                                           4,
                                           tabWidth,
                                           tabHeight)];
            @try {
                [button setImage:[UIImage imageNamed:
                                  [tabImageNamesNormal objectAtIndex:i]] 
                        forState:UIControlStateNormal];
            }
            @catch (NSException *exception) {
                NSLog(@"No normal-state image for tabBar button at index %i" ,i);
            }
            @try {
                [button setImage:[UIImage imageNamed:
                                  [tabImageNamesSelected objectAtIndex:i]] 
                        forState:UIControlStateSelected];
            }
            @catch (NSException *exception) {
                NSLog(@"No selected-state image for tabBar button at index %i" ,i);
            }
            
           
            
            [button addTarget:self action:@selector(tabTouched:) 
             forControlEvents:UIControlEventTouchUpInside];
         
            [button setAdjustsImageWhenHighlighted:NO];
            
            button.autoresizingMask= (UIViewAutoresizingFlexibleWidth |
             UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin);
            
            /*
             * buttons tags are numbered with the same
             * order as they appear in tabbar.in order
             * to user sender.tag as an indecation of 
             * which button is touched in buttonTouched:
             * action
             */
            [button setTag:i];
            
            [bgView addSubview:button];
                // [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"footer_Bg"]]];
                //  [self.tabBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"footer_Bg"]]];

                //  [self.tabBarController.tabBar  setBackgroundColor:[UIColor redColor]];//[UIColor colorWithPatternImage:[UIImage imageNamed:@"footer_Bg"]]];

            [temp addObject:button];
            
            button = nil;
        }
       
        self.tabs = temp;
        
        [self setViewControllers:viewControllers];
    }
    return self;
}



-(void)setSelectedIndex:(NSUInteger)selectedIndex 
{
    for (UIButton *tab in self.tabs) {
        if (tab.tag != selectedIndex) {
            [tab setSelected:NO];
        }
        else {
            [tab setSelected:YES];
        }
    }
    [self setSelectedViewController:
     [self.viewControllers objectAtIndex:selectedIndex]];
}


#pragma mark - Actions


-(IBAction)tabTouched:(UIButton*)sender 
{

    for (UIButton *tab in self.tabs) {
        if (tab.tag != sender.tag) {
            [tab setSelected:NO];
        }
        else {
            [tab setSelected:YES];
        }
    }
    if ([sender tag]==2 || [sender tag]==4) {
        [[self.viewControllers objectAtIndex:sender.tag] popToRootViewControllerAnimated:YES];
    }
   [self setSelectedIndex:sender.tag];
}




#pragma mark - bult-in functions

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
     return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft)|| (interfaceOrientation == UIInterfaceOrientationLandscapeRight) || (interfaceOrientation == UIInterfaceOrientationPortrait));
}

- (void)dealloc
{
    [self setTabs:nil];
}

-(void)orientationDidCahnged:(NSNotification*)object{
   /* float tabWidth =80;
       
    UIInterfaceOrientation orientation=[[UIApplication sharedApplication]statusBarOrientation];
    
    if (orientation==UIDeviceOrientationPortrait ||
        orientation==UIDeviceOrientationPortraitUpsideDown ) {
        tabWidth = self.view.frame.size.width/ self.tabs.count;
    }else{
        tabWidth=self.view.frame.size.height/ self.tabs.count;
    }
    NSLog(@"orientation : %i , is portrait : %d" ,orientation , (orientation==UIDeviceOrientationPortrait ||
          orientation==UIDeviceOrientationPortraitUpsideDown));
    NSLog(@"view frame: %f , %f , %f , %f",self.view.frame.origin.x,self.view.frame.origin.y , self.view.frame.size.width , self.view.frame.size.height);
    NSLog(@"tab width : %f ",tabWidth);
    for (UIButton * btn  in self.tabs) {
        [btn setFrame:CGRectMake(btn.tag*tabWidth, 0, tabWidth, btn.frame.size.height)];
    }
*/
}

/*
 * for iOS 6
 */
-(void)didReceiveMemoryWarning
{
    [self setTabs:nil];
}

@end
