//
//  DatePickerView.h
//  Ramadnyat
//
//  Created by Ali Amin on 8/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DatePickerViewDelegate <NSObject>

-(void)DatePickerNewAlarmSettingFinished:(BOOL)succeeded;

@end

@interface DatePickerView : UIView{
    TopicModel* currentTopic;
    
    __weak IBOutlet UIView *portraitView;
    __weak IBOutlet UIView *lndScpView;
}


@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *lndDatePicker;


@property (nonatomic , weak) id<DatePickerViewDelegate> delegate;

+(DatePickerView *)LoadNibInController:(UIViewController *)controller;


+(NSString*)getNibName;
-(void)showForTopic:(TopicModel*)topic;

- (IBAction)OkButtonTouched:(id)sender;
- (IBAction)cancelButtonTouched:(id)sender;
@end
