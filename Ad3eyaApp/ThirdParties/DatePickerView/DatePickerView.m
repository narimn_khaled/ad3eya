//
//  DatePickerView.m
//  Ramadnyat
//
//  Created by Ali Amin on 8/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "DatePickerView.h"
#import "FTAnimation+UIView.h"

@implementation DatePickerView
@synthesize delegate;



+(DatePickerView *)LoadNibInController:(UIViewController *)controller{
    NSString * nibname=@"";
     nibname= DEVICE_IS_PHONE? @"DatePickerView":@"DatePickerView_iPad";
    
    DatePickerView * picker=[[NSBundle mainBundle]loadNibNamed:nibname  owner:controller options:nil][0];
    [picker orientationDidCahnged:nil];
    
    return picker;
}


+(NSString *)getNibName{
    NSString * nibName;
    UIInterfaceOrientation ori=[[UIApplication sharedApplication]statusBarOrientation];
    if (DEVICE_IS_PHONE) {
        if(DEVICE_IS_IPHONE5){
            nibName= (ori==UIInterfaceOrientationPortrait)? @"DatePickerView5":@"DatePickerView_lnd5";
        }else{
             nibName= (ori==UIInterfaceOrientationPortrait)? @"DatePickerView":@"DatePickerView_lnd";
        }
    }else{
         nibName= (ori==UIInterfaceOrientationPortrait)?@"DatePickerView_iPad":@"DatePickerView_iPad_lnd";
    }
    NSLog(@"datePickerView nibName: %@", nibName);
    return nibName;
}

-(void)showLandScapeView{
    [portraitView setHidden:YES];
    [lndScpView setHidden:YES];
}

-(void)orientationDidCahnged:(NSNotification*)object{
    UIInterfaceOrientation ori=[[UIApplication sharedApplication]statusBarOrientation];
    if (ori == UIInterfaceOrientationPortrait) {
        [portraitView setHidden:NO];
        [lndScpView setHidden:YES];
       if(DEVICE_IS_PHONE) [self.datePicker setDate:self.lndDatePicker.date];
    }else{
        [portraitView setHidden:YES];
        [lndScpView setHidden:NO];
        if(DEVICE_IS_PHONE) [self.lndDatePicker setDate:self.datePicker.date];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

-(void)showForTopic:(TopicModel *)topic {
  //  self.autoresizingMask=UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(orientationDidCahnged:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    
    [self.lndDatePicker setTransform:CGAffineTransformMakeScale(1, .9)];
    [self orientationDidCahnged:nil];
    
    currentTopic=topic;
    if (currentTopic.haveAlarm) {
        [self.datePicker setDate:currentTopic.alarmNotification.fireDate];//alarmDate];
        [self.lndDatePicker setDate:currentTopic.alarmNotification.fireDate];
    }
    
    [self slideInFrom:kFTAnimationBottom duration:.3 delegate:nil];
}



- (IBAction)OkButtonTouched:(id)sender {
    //make the local notifiction

    [self setLocalNotifications];

    if ([delegate respondsToSelector:@selector(DatePickerNewAlarmSettingFinished:)]) {
        [delegate DatePickerNewAlarmSettingFinished:YES];
    }
    [self slideOutTo:kFTAnimationBottom duration:.3 delegate:nil startSelector:nil stopSelector:@selector(removeFromSuperview)];
}

- (IBAction)cancelButtonTouched:(id)sender {
    if ([delegate respondsToSelector:@selector(DatePickerNewAlarmSettingFinished:)]) {
        [delegate DatePickerNewAlarmSettingFinished:NO];
        }
    [self slideOutTo:kFTAnimationBottom duration:.3 delegate:nil startSelector:nil stopSelector:@selector(removeFromSuperview)];

}

-(void)removeSelfFromSuperView{
    [self removeFromSuperview];
}



-(void)setLocalNotifications{
    UIInterfaceOrientation ori=[[UIApplication sharedApplication]statusBarOrientation];
    NSDate * date=DEVICE_IS_PHONE?(  ori==UIInterfaceOrientationPortrait?self.datePicker.date : self.lndDatePicker.date):self.datePicker.date;
    [[appDataManger SharedInstence]setAlarmForTopic:currentTopic inDate:date];
   
    /*
    NSDate *itemDate = [self.datePicker date];
    
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    
    localNotif.repeatInterval = NSDayCalendarUnit;
    localNotif.fireDate = itemDate;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    // Notification details
    
    localNotif.alertBody =DoaaString; //[eventText text];
    // Set the action button
    localNotif.alertAction =NSLocalizedString(@"alertCancel", nil);
    
    localNotif.soundName = UILocalNotificationDefaultSoundName; //@"azan.caf";
    //localNotif.applicationIconBadgeNumber =[[UIApplication sharedApplication]applicationIconBadgeNumber]+1;
    /*
     // Specify custom data for the notification
     NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"someValue" forKey:@"someKey"];
     localNotif.userInfo = infoDict;
     
    // Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    */
   
    
}


@end
