//
//  NetworkOperations.m
//  NetworkOperations
//
//  Created by Ahmad al-Moraly on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AFNetworking.h"

//#import "XMLDictionary.h"

#import "NetworkOperations.h"
#import "JSONKit.h"

/**
 * The baseURl to be used in constructing all the requests.
 *
 * change this to be the name of the Base URL of your server.
 */
NSString* const kNetworkBaseURLString=@"http://demo.byscripts.com/iphoneweb.php";//http://izwaj.com/u/u.php";//@"http://m7lat.net/hany/iphoneweb.php";//


@interface NetworkOperations ()

+(NSMutableURLRequest *)requestWithMethod:(HTTPRequestMethod)requestMethod
                            andParameters:(NSDictionary *)parameters;

+(NSMutableURLRequest *)requestWithPath:(NSString *)path method:(HTTPRequestMethod)requestMethod
                            andParameters:(NSDictionary *)parameters;


+(AFHTTPRequestOperation *)operationWithRequest:(NSURLRequest *)request
                                       progress:(void (^)(NSInteger, NSInteger, NSInteger))progress
                                        success:(void (^)(id))success
                                        failure:(void (^)(NSError *))failure;

+(NSMutableURLRequest *)imageUploadRequestWithImage:(UIImage *)image
                                           withName:(NSString *)name
                                               path:(NSString *)url
                                         parameters:(NSDictionary *)parameters;

+(NetworkOperations *)sharedClient;

@end

@implementation NetworkOperations


+(void)startReachabilityNotificationWithObserver:(id)observer andSelector:(SEL)selector {
    [self sharedClient];
    [[NSNotificationCenter defaultCenter] addObserver:observer selector:selector name:AFNetworkingOperationDidStartNotification object:nil];
}


+(void)stopReachabilityNotificationWithObserver:(id)observer {
    [[NSNotificationCenter defaultCenter] removeObserver:observer name:AFNetworkingOperationDidStartNotification object:nil];
}

#pragma mark - 
#pragma mark - Public API
+(AFHTTPRequestOperation *)operationWithParamerters:(NSDictionary *)parameters requestMethod:(HTTPRequestMethod)requestMethod andSuccessBlock:(void (^)(id))success {
    NSMutableURLRequest *request = [self requestWithMethod:requestMethod andParameters:parameters];
    return [self operationWithRequest:request progress:nil success:success failure:nil];
}

+(AFHTTPRequestOperation *)operationWithParamerters:(NSDictionary *)parameters requestMethod:(HTTPRequestMethod)requestMethod successBlock:(void (^)(id))success andFailureBlock:(void (^)(NSError *))failure {
    
    NSMutableURLRequest *request = [self requestWithMethod:requestMethod andParameters:parameters];
    return [self operationWithRequest:request progress:nil success:success failure:failure];
}

+(AFHTTPRequestOperation *)operationWithParamerters:(NSDictionary *)parameters requestMethod:(HTTPRequestMethod)requestMethod progressBlock:(void (^)(NSInteger, NSInteger, NSInteger))progress successBlock:(void (^)(id))success andFailureBlock:(void (^)(NSError *))failure {
    
    NSMutableURLRequest *request = [self requestWithMethod:requestMethod andParameters:parameters];
    return [self operationWithRequest:request progress:progress success:success failure:failure];
    
}

+(AFHTTPRequestOperation *)operationWithFullURL:(NSString *)url parameters:(NSDictionary *)parameters requestMethod:(HTTPRequestMethod)requestMethod successBlock:(void (^)(id))success andFailureBlock:(void (^)(NSError *))failure {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
     
    NSString* parametersString=@"";
    NSString* stringToAppend=@"";
    for (int i=0; i<[parameters count]; i++) {
        if(i==[parameters count]-1){
            stringToAppend=[NSString stringWithFormat:@"%@=%@",[[parameters allKeys]objectAtIndex:i ],[parameters objectForKey:[[ parameters allKeys]objectAtIndex:i ]]];
        }else {
                stringToAppend=[NSString stringWithFormat:@"%@=%@&",[[parameters allKeys]objectAtIndex:i ],[parameters objectForKey:[[ parameters allKeys]objectAtIndex:i ]]];
        }
        NSLog(@"string to append: %@ ",stringToAppend);
        parametersString=[parametersString stringByAppendingString:stringToAppend];
    }
       
    NSData *parametersData = [parametersString dataUsingEncoding: NSUTF8StringEncoding];
    
    [request setHTTPBody:parametersData];
    switch (requestMethod) {
        case HTTPRequestMethodGET:
            [request setHTTPMethod:@"GET"];
            break;
        case HTTPRequestMethodPOST:
            [request setHTTPMethod:@"POST"];
        default:
            break;
    }
    
    return [self operationWithRequest:request progress:nil success:success failure:failure];
}

+(AFHTTPRequestOperation *)operationWithPath:(NSString *)url parameters:(NSDictionary *)parameters requestMethod:(HTTPRequestMethod)requestMethod successBlock:(void (^)(id))success andFailureBlock:(void (^)(NSError *))failure
{
    NSMutableURLRequest *request = [self requestWithPath:url method:requestMethod andParameters:parameters];
    return [self operationWithRequest:request progress:nil success:success failure:failure];
}
/*
 two functions commented in the following code , please pay attention 
 */

+(AFHTTPRequestOperation *)uploadImage:(UIImage *)image withName:(NSString *)name path:(NSString *)url parameters:(NSDictionary *)parameters progress:(void (^)(NSInteger, NSInteger, NSInteger))progress success:(void (^)(id))success andFailure:(void (^)(NSError *))failure {
    
         NSMutableURLRequest *request = [self imageUploadRequestWithImage:image withName:name path:url parameters:parameters];
    
    return  [self operationWithRequest:request progress:progress success:success failure:failure];
}

+(void)uploadImages:(NSArray *)arrayOfImages withName:(NSString *)name path:(NSString *)url progress:(void (^)(NSInteger, NSInteger))progress success:(void (^)(id))success andFailure:(void (^)(NSArray *))failure {
    
    // create images requests
    NSMutableArray *requestsArray = [NSMutableArray array];
    [arrayOfImages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
               [requestsArray addObject:[self imageUploadRequestWithImage:obj withName:name path:url parameters:nil]];
    }];
    
    [[self sharedClient] enqueueBatchOfHTTPRequestOperationsWithRequests:requestsArray progressBlock:^(NSUInteger numberOfCompletedOperations, NSUInteger totalNumberOfOperations) {
        NSLog(@"[NetworkOperations Finished Uploading: %d of %d images]", numberOfCompletedOperations, totalNumberOfOperations);
        if (progress) {
            progress(numberOfCompletedOperations, totalNumberOfOperations);
        }
        
    } completionBlock:^(NSArray *operations) {
        __block NSMutableArray *response = [NSMutableArray array];
        [operations enumerateObjectsUsingBlock:^(AFHTTPRequestOperation *operation, NSUInteger idx, BOOL *stop) {
            
            if (operation.error) {
                // failure
                // if (TURN_LOGGER_ON) 
                    NSLog(@"[NetworkOperations Downloading ERROR] %@\n", operation.error);
                
                [response addObject:operation.error];
            } else {
                // success
                
                    //    if (TURN_LOGGER_ON) 
                    NSLog(@"[NetworkOperations Uploading Response: %@\n For Image With URL: %@\n]", operation.responseString, operation.request.URL);
                [response addObject:operation.responseString];
            }
        }];
        if (response.count > 0) {
            if (failure) {
                failure(response);
            }
        } else {
            if (success) {
                success(nil);
            }
        }
    }];
}

+(void)uploadImages:(NSArray *)arrayOfImages withName:(NSString *)name path:(NSString *)url parameters:(NSDictionary *)parameters progress:(void (^)(NSInteger, NSInteger))progress success:(void (^)(id))success andFailure:(void (^)(NSArray *))failure{
    __block BOOL dataSend=NO;
        // create images requests
    NSMutableArray *requestsArray = [NSMutableArray array];
    [arrayOfImages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (dataSend) {
              [requestsArray addObject:[self imageUploadRequestWithImage:obj withName:name path:url parameters:nil]];
        }
        else {
             [requestsArray addObject:[self imageUploadRequestWithImage:obj withName:name path:url parameters:parameters]];
            dataSend=YES;
        }
    }];
    
    [[self sharedClient] enqueueBatchOfHTTPRequestOperationsWithRequests:requestsArray progressBlock:^(NSUInteger numberOfCompletedOperations, NSUInteger totalNumberOfOperations) {
        NSLog(@"[NetworkOperations Finished Uploading: %d of %d images]", numberOfCompletedOperations, totalNumberOfOperations);
        if (progress) {
            progress(numberOfCompletedOperations, totalNumberOfOperations);
        }
        
    } completionBlock:^(NSArray *operations) {
        __block NSMutableArray * succesMArray=[NSMutableArray array];
        __block NSMutableArray *errorResponse = [NSMutableArray array];
        [operations enumerateObjectsUsingBlock:^(AFHTTPRequestOperation *operation, NSUInteger idx, BOOL *stop) {
            
            if (operation.error) {
                    // failure
                    //if (TURN_LOGGER_ON) 
                    NSLog(@"[NetworkOperations Downloading ERROR] %@\n", operation.error);
                
                [errorResponse addObject:operation.error];
            } else {
                    // success
                
                    //  if (TURN_LOGGER_ON) 
                    NSLog(@"[NetworkOperations Uploading Response: %@\n For Image With URL: %@\n]", operation.responseString, operation.request.URL);
                
                  [succesMArray addObject:operation.responseString];
            }
        }];
        if (errorResponse.count > 0) {
                //  if (failure) {
                failure(errorResponse);
                //  }
               } else {
                       //   if (success) {
                success(succesMArray);
                       //  }
               }
    }];
}

#pragma mark -
#pragma mark - Private API

+(NSMutableURLRequest *)requestWithMethod:(HTTPRequestMethod)requestMethod andParameters:(NSDictionary *)parameters {
    NSMutableURLRequest *request;
    switch (requestMethod) {
        case HTTPRequestMethodGET:
            request = [[self sharedClient] requestWithMethod:@"GET" path:@"" parameters:parameters];
            break;
            
        case HTTPRequestMethodPOST:
            request = [[self sharedClient] requestWithMethod:@"POST" path:@"" parameters:parameters];
            break;
        default:
            request = nil;
            break;
    }
    
    return request;
}

+(NSMutableURLRequest *)requestWithPath:(NSString *)path method:(HTTPRequestMethod)requestMethod andParameters:(NSDictionary *)parameters {
    NSMutableURLRequest *request;
    switch (requestMethod) {
        case HTTPRequestMethodGET:
            request = [[self sharedClient] requestWithMethod:@"GET" path:path parameters:parameters];
            break;
            
        case HTTPRequestMethodPOST:
            request = [[self sharedClient] requestWithMethod:@"POST" path:path parameters:parameters];
            break;
        default:
            request = nil;
            break;
    }
    
    return request;
}

+(NSMutableURLRequest *)imageUploadRequestWithImage:(UIImage *)image withName:(NSString *)name path:(NSString *)url parameters:(NSDictionary *)parameters {
    
    NSString *mimeType;
    NSData *imageData = UIImagePNGRepresentation(image);
    mimeType = @"image/png";
    if (!imageData) {
        imageData = UIImageJPEGRepresentation(image, 1.0);
        mimeType = @"image/jpg";
    }
    
    NSMutableURLRequest *request = [[self sharedClient] multipartFormRequestWithMethod:@"POST" path:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData name:name fileName:@"uploadedImage.png" mimeType:mimeType];
    }];
    
    return request;
}
+(AFHTTPRequestOperation *)operationWithRequest:(NSURLRequest *)request progress:(void (^)(NSInteger, NSInteger, NSInteger))progress success:(void (^)(id))success failure:(void (^)(NSError *))failure {
    
    AFHTTPRequestOperation *operation = [[self sharedClient] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
            if (TURN_LOGGER_ON) 
            NSLog(@"[NetworkOperations Downloaded Response] %@\n", operation.responseString);

        if ([operation isKindOfClass:[AFJSONRequestOperation class]]) {
            // response was JSON no need to parse
                        
            if ([responseObject objectForKey:@"error"]) {
                    // error
                NSError *error = [NSError errorWithDomain:@"NetworkOperationJSONError" code:0 userInfo:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"JSON did fail with error: %@", [responseObject objectForKey:@"error"]] forKey:NSLocalizedDescriptionKey]];
                if (TURN_LOGGER_ON) NSLog(@"[NetworkOperations Response ERROR] %@\n", error);

                if (failure) {
                    failure(error);
                }
                
            } else {
                if (TURN_LOGGER_ON) NSLog(@"[NetworkOperations Parsed JSON Response] %@\n", responseObject);
                
                if (success) {
                    success(responseObject);
                }
            }
            
        }
        else{
            JSONDecoder *json=[[JSONDecoder alloc]init];
            responseObject=  [json objectWithUTF8String:(const unsigned char *) [operation.responseString UTF8String ] length:operation.responseString.length];
                //  NSLog(@"%@",responseObject);
            
                success(responseObject);
            
        }
        
    } 
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (TURN_LOGGER_ON) NSLog(@"[NetworkOperations Downloading ERROR] %@\n", error);
        if (failure) {
            failure(error);
        }

    }];
    
    [operation setDownloadProgressBlock:^(NSInteger bytesRead, NSInteger totalBytesRead, NSInteger totalBytesExpectedToRead) {
        if (TURN_LOGGER_ON) NSLog(@"[NetworkOperations downloaded: %d of %d bytes]", totalBytesRead, totalBytesExpectedToRead);
            
        if (progress) {
            progress(bytesRead, totalBytesRead, totalBytesExpectedToRead);
        }
    }];
    
    [[self sharedClient] enqueueHTTPRequestOperation:operation];
    [operation addObserver:[self sharedClient] forKeyPath:@"isExecuting" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
    
    return operation;

}


+(AFHTTPRequestOperation *)downloadFileWithURL:(NSString *)url withParameters:(NSDictionary *)parameters progress:(void (^)(NSInteger, NSInteger, NSInteger))progress fileSavingName:(NSString *)name success:(void (^)(NSString *filePath))success failure:(void (^)(NSError *))failure {

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    AFHTTPRequestOperation *operation = [[self sharedClient] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject)
                 {
                     NSLog(@"[Network Operations File Downloaded Successfully]");
                     // prepare file path to save
                     
                     NSFileManager *fileManager = [NSFileManager defaultManager];
                     NSString *saveDirectory = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Downloads"];
                     if (![fileManager fileExistsAtPath:saveDirectory]) {
                         [fileManager createDirectoryAtPath:saveDirectory withIntermediateDirectories:YES attributes:nil error:nil];
                     }
                     
                     NSString *filename = (name) ? [name stringByAppendingPathExtension:operation.response.URL.pathExtension] : [operation.response.URL lastPathComponent];
                     NSString *absolutePath = [saveDirectory stringByAppendingPathComponent:filename];
                     
                     if ([fileManager fileExistsAtPath:absolutePath]) {
                         absolutePath = [saveDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@", filename, [NSDate date]]];
                     }
                     
                     if ([fileManager createFileAtPath:absolutePath contents:operation.responseData attributes:nil]) {
                         NSLog(@"File Saved to %@", absolutePath);
                         if (success) {
                             success(absolutePath);
                         }
                     }

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) 
    {
        NSLog(@"[Network Operations ERROR]: %@", error.description);
        if (failure) {
            failure(error);
        }
    }];

    
    [operation setAcceptableContentTypes:[NSSet setWithObjects:@"application/pdf", @"application/xml", @"audio/mp4a-latm", @"audio/mpeg", @"audio/mp3", @"video/x-m4v", @"video/mp4", nil]];
    
    [operation setDownloadProgressBlock:^(NSInteger bytesRead, NSInteger totalBytesRead, NSInteger totalBytesExpectedToRead) {
        NSLog(@"[Downloaded %d of %d bytes]", totalBytesRead, totalBytesExpectedToRead);
        if (progress) {
            //dispatch_async(dispatch_get_main_queue(), ^{
            progress(bytesRead, totalBytesRead, totalBytesExpectedToRead);
            //});
            
        }
    }];
    
    [[self sharedClient] enqueueHTTPRequestOperation:operation];
    return operation;
}

+(NetworkOperations *)sharedClient {
    static NetworkOperations *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:kNetworkBaseURLString]];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
    }   
    // Accept HTTP Header; see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
	[self setDefaultHeader:@"Accept" value:@"application/json"];
	[self setDefaultHeader:@"Accept" value:@"text/json"];
	[self setDefaultHeader:@"Accept" value:@"text/javascript"];
    [self setDefaultHeader:@"Accept" value:@"text/*"];
    
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
        //[self setAuthorizationHeaderWithUsername:@"user1" password:@"234567"];
    
    return self;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if ([object isKindOfClass:[NSOperation class]] && [keyPath isEqualToString:@"isExecuting"]) {
        AFURLConnectionOperation *operation = object;
        if (operation.isExecuting) {
            NSString *body = nil;
            if (operation.request.HTTPBody) {
                body = [[NSString alloc] initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding];
            }

            NSLog(@"[Network Operations Start Operation With URL: %@ Parameters: %@ Request Method: %@]", operation.request.URL, body, operation.request.HTTPMethod);   
            
            [body release];         
        }
        else if (operation.isFinished || operation.isCancelled) {
            [operation removeObserver:self forKeyPath:keyPath];
        }
    }
}


@end

