//
//  appDataManger.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/26/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TopicModel.h"
#import <Social/Social.h>

@interface appDataManger : NSObject <UIActionSheetDelegate>
{
    NSMutableDictionary * _data;
    NSMutableDictionary * _dataOrder;
    NSMutableDictionary * _categories;
    NSMutableArray * _categoriesIDs;
    //topic sharing
    SLComposeViewController *mySLComposerSheet;
    TopicModel* TopicToBeShared;
    UIViewController * controllerToShareIn;
    
    
}


@property(retain , nonatomic) NSMutableDictionary * Data;
@property (retain , nonatomic )NSMutableDictionary * DataOrder;
@property(retain , nonatomic) NSMutableDictionary * Categories;
@property (retain , nonatomic )NSMutableArray * CategoriesIDs;

+(id)SharedInstence;

-(NSMutableArray*)getTopicsOfCategoryID:(int)categoryID;
-(TopicModel*)getTopicData:(NSString*)topicID;
-(void)AddRemoveTopicToFavorites:(TopicModel*)topic;
-(NSMutableArray *)getFavoriteTopicsData;
-(NSMutableArray *)getAlarmTopicsData;
-(void)setAlarmForTopic:(TopicModel*)topic inDate:(NSDate*) alarmDate;
-(void)RemoveAlarmForTopic:(TopicModel*)topic;
-(void)shareTopic:(TopicModel*)selectedTopic inViewController:(UIViewController*)controller;
- (void)facebookShareForTopic:(TopicModel*)selectedTopic inViewController:(UIViewController*)controller;
-(void)shareAtTwitterWithTopic:(TopicModel*)selectedTopic inViewController:(UIViewController*)controller;
@end
