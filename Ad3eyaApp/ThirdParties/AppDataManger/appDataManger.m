//
//  appDataManger.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/26/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "appDataManger.h"
#import "JSONKit.h"
#import "AppSharer.h"
#import "TwitterShare.h"

#define FavTopics @"FAVS"
#define AlarmTopics @"Alarm"

@implementation appDataManger
@synthesize Data=_data;
@synthesize DataOrder=_dataOrder;
@synthesize Categories=_categories;
@synthesize CategoriesIDs=_categoriesIDs;

+(id)SharedInstence{
    static appDataManger *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[self alloc] init];
    });
    
    return _sharedClient;
}



- (id)init
{
    self = [super init];
    if (self) {
        [self loadContentsOfFile];
    }
    return self;
}


-(void)loadContentsOfFile{
    NSError *error = nil;
    NSString* path = [[NSBundle mainBundle] pathForResource:@"DataText" ofType:@"txt"];
    NSString* FileContents = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    
    NSDictionary* dictionaryData = [FileContents objectFromJSONString];
   // NSLog(@"calss : %@ && dictionary : %@",[dictionaryData class], dictionaryData);
    
    _data=[[NSMutableDictionary alloc]init];
    _dataOrder=[NSMutableDictionary new];
    _categories =[NSMutableDictionary new];
    _categoriesIDs=[NSMutableArray new];
    
    NSMutableArray * favsIds=[self getFavoriteTopicsIDs];
    
    NSMutableDictionary * alarmData=[self getAlarmsData];
    
    for (NSDictionary* dic in dictionaryData) {
        TopicModel* topic=[[TopicModel alloc]initWithDictionary:dic];
        if (![_categoriesIDs containsObject:topic.categoryID]) {
                [_categories setObject:[NSDictionary dictionaryWithObjectsAndKeys:topic.categoryName,@"name",topic.categoryOrder,@"order", nil ] forKey:topic.categoryID];
            [_categoriesIDs addObject:topic.categoryID];
            }

        if ([favsIds containsObject:topic.topiccID]) {
            topic.isFav=YES;
        }
        
        if ([alarmData objectForKey:topic.topiccID]!=nil) {
            topic.haveAlarm=YES;
            topic.alarmNotification=[alarmData objectForKey:topic.topiccID];
        }
        if ([topic.topiccID isEqualToString:@"200"]) {
            NSLog(@"the 200 object ");
        }
        [_data setObject:topic forKey:topic.topiccID];
        NSMutableDictionary * topicsInCat=[_dataOrder objectForKey:topic.categoryID];
        if (!topicsInCat) {
            topicsInCat=[[NSMutableDictionary alloc]init];
        }
        
        //[topicsInCat insertObject:topic.topiccID atIndex:[topic.topicOrder integerValue]];
        [topicsInCat setObject:topic.topiccID forKey:[NSNumber numberWithInt: topic.topicOrder]];//addObject:[NSDictionary dictionaryWithObjectsAndKeys: topic.topiccID, topic.topicOrder , nil]];
        [_dataOrder setObject:topicsInCat forKey:topic.categoryID];
    }
    NSLog(@"categories : %@" , _categories);
    NSLog(@"data order : %@", _dataOrder);
    NSLog(@"topics IDs : %@" , [_data allKeys]);
}




-(NSMutableArray*)getTopicsOfCategoryID:(int)categoryID{
    NSMutableArray* resultArray=[NSMutableArray new];
   
    NSMutableDictionary *topicsIDsForCat=[_dataOrder objectForKey:[NSString stringWithFormat:@"%i", categoryID ]];
    NSLog(@"topics IDs For Cat = %@",topicsIDsForCat);
    
    for (int i=1; i<=[topicsIDsForCat count]; i++) {
        TopicModel * topic=[_data objectForKey:[topicsIDsForCat objectForKey:[NSNumber numberWithInt: i ]]];
        if (topic) {
             [resultArray addObject:topic];
        }else{
            NSLog(@"empty item at index %i",i);
        }
    
    }
    
 /*
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"order"  ascending:YES];
    [topicsIDsForCat sortUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    NSLog(@"\n sort method : %@ ", topicsIDsForCat);
    */
    /*
    for (NSDictionary * topicID in topicsIDsForCat) {
        [resultArray addObject:[_data objectForKey:[topicID objectForKey:@"id"]]];
    }*/
    
   /* for (TopicModel *topic in [_data allValues ]) {
        if ([topic.categoryID isEqualToString:[NSString stringWithFormat: @"%i", categoryID]]) {
            [resultArray addObject:topic];
        }
    }*/
    return resultArray;
}


#pragma -mark favorites Functions
-(NSMutableArray *)getFavoriteTopicsIDs{
    NSMutableArray * favsIds=[[[NSUserDefaults standardUserDefaults]objectForKey:FavTopics] mutableCopy];
    if (favsIds ==nil) {
        favsIds=[NSMutableArray new];
    }
    return favsIds;
}

-(NSMutableArray *)getFavoriteTopicsData{
    NSMutableArray * Ids =[self getFavoriteTopicsIDs];
    NSLog(@"favs array : %@ ", Ids);
    NSMutableArray * favsData=[NSMutableArray new];
    for (NSString* ID in Ids) {
        if ([[_data allKeys]containsObject:ID]) {
             [favsData addObject:[_data objectForKey:ID]];
        }
    }
    return favsData;
}

-(void)AddRemoveTopicToFavorites:(TopicModel*)topic{
    NSMutableArray * favsIds=[self getFavoriteTopicsIDs];
    // TopicModel* selectedTopic= [_data objectForKey:topic.topiccID];;
    if (topic.isFav) {
        [favsIds removeObject:topic.topiccID];
        topic.isFav=NO;
    }else{
        [favsIds addObject:topic.topiccID];
        topic.isFav=YES;
    }
    [[NSUserDefaults standardUserDefaults]setObject:favsIds forKey:FavTopics];
    [[NSUserDefaults standardUserDefaults]synchronize];
}




#pragma -mark Alarm functions

-(NSMutableArray *)getAlarmTopicsData{
    NSMutableArray * alarmData=[NSMutableArray new];
    NSMutableArray*  alarmsArray=  [[[UIApplication sharedApplication] scheduledLocalNotifications] mutableCopy];
    for (UILocalNotification* notif in alarmsArray) {
        if ([[_data allKeys]containsObject:[notif.userInfo objectForKey:@"id"]]) {
            [alarmData addObject:[_data objectForKey:[notif.userInfo objectForKey:@"id"]]];
        }else{
            [[UIApplication sharedApplication]cancelLocalNotification:notif];
        }
        
        //  [alarmData setObject:[NSDictionary dictionaryWithObject:notif.fireDate forKey:@"date"] forKey:[notif.userInfo objectForKey:@"id"]];
    }
    return alarmData;
    
}

-(NSMutableDictionary*)getAlarmsData{
    // return  [[[NSUserDefaults standardUserDefaults]objectForKey:AlarmTopics]mutableCopy];
    
   // [[UIApplication sharedApplication]cancelAllLocalNotifications];
    NSMutableDictionary* alarmData=[NSMutableDictionary new];
    NSMutableArray*  alarmsArray=  [[[UIApplication sharedApplication] scheduledLocalNotifications] mutableCopy];
    NSLog(@"notifications data : %@",alarmsArray);
    for (UILocalNotification* notif in alarmsArray) {
        //[NSDictionary dictionaryWithObject:notif.fireDate forKey:@"date"]
        [alarmData setObject:notif forKey:[notif.userInfo objectForKey:@"id"]];
    }
    return alarmData;
}


-(void)setAlarmForTopic:(TopicModel*)topic inDate:(NSDate*) alarmDate {
    if (topic.haveAlarm) {
      //  [topic.alarmNotification setFireDate:alarmDate];
        [[UIApplication sharedApplication]cancelLocalNotification:topic.alarmNotification];
                
    }
        
        UILocalNotification *localNotif = [[UILocalNotification alloc] init];
        if (localNotif == nil)
            return;
        
        localNotif.repeatInterval = NSDayCalendarUnit;
        localNotif.fireDate = alarmDate;
        localNotif.timeZone = [NSTimeZone defaultTimeZone];
        
        // Notification details
        localNotif.alertBody =topic.topicTitle;
        localNotif.alertAction =@"تم";
        localNotif.soundName = UILocalNotificationDefaultSoundName; //@"azan.caf";
        
        // Specify custom data for the notification
        NSDictionary *infoDict = [NSDictionary dictionaryWithObject:topic.topiccID forKey:@"id"];
        localNotif.userInfo = infoDict;
        
        // Schedule the notification
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    
    if (topic.haveAlarm) {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"تم" message:@"تم تعديل التنبيه بنجاح" delegate:nil cancelButtonTitle:@"تم" otherButtonTitles: nil];
        [alert show];

    }else{
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"تم" message:@"تم إضافة التنبيه بنجاح" delegate:nil cancelButtonTitle:@"تم" otherButtonTitles: nil];
        [alert show];

    }
    
        topic.haveAlarm=YES;
        topic.alarmDate=alarmDate;
        topic.alarmNotification=localNotif;
        
           
}

-(void)RemoveAlarmForTopic:(TopicModel *)topic{
    if (topic.alarmNotification !=nil) {
          [[UIApplication sharedApplication]cancelLocalNotification:topic.alarmNotification];
    }
    topic.alarmNotification=nil;
    topic.alarmDate=nil;
    topic.haveAlarm=NO;
    
    UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"تم" message:@"تم الغاء التنبيه بنجاح" delegate:nil cancelButtonTitle:@"تم" otherButtonTitles: nil];
    [alert show];
}


#pragma -mark sharing


-(void)shareTopic:(TopicModel *)selectedTopic inViewController:(UIViewController *)controller{
    TopicToBeShared=selectedTopic;
    controllerToShareIn=controller;
    UIActionSheet *sheet=[[UIActionSheet alloc]initWithTitle:@"نشر على" delegate:self cancelButtonTitle:@"إلغاء" destructiveButtonTitle:nil otherButtonTitles:@"فيس بوك",@"تويتر", nil];
    [sheet showFromTabBar:tabBar.tabBar ];
}


-(void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self facebookShareForTopic:TopicToBeShared inViewController:controllerToShareIn];
            break;
        case 1:
            [[appDataManger SharedInstence]shareAtTwitterWithTopic:TopicToBeShared inViewController:controllerToShareIn];
            break;
        default:
            break;
    }
    TopicToBeShared=nil;
    controllerToShareIn=nil;
}

#pragma -mark facebook ios6 sharing




- (void)facebookShareForTopic:(TopicModel*)selectedTopic inViewController:(UIViewController*)controller {
    
    if ([[[UIDevice currentDevice] systemVersion] intValue]>=6) {
        
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
        {
            mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
            mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook]; //Tell him with what social plattform to use it, e.g. facebook or twitter
            [mySLComposerSheet setInitialText:[NSString stringWithFormat:@"%@  ",  selectedTopic.topicTitle]];
            [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                NSString *output=@"";
                switch (result) {
                    case SLComposeViewControllerResultDone:
                        output = @"تم النشر";
                        break;
                    default:
                        break;
                } //check if everythink worked properly. Give out a message on the state.
                if (output.length>0) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"فيس بوك" message:output delegate:nil cancelButtonTitle:@"تم" otherButtonTitles:nil];
                    [alert show];
                }
                
            }];
            
            //the message you want to post
            //[mySLComposerSheet addImage:nil];
            //an image you could post
            //for more instance methodes, go here:https://developer.apple.com/library/ios/#documentation/NetworkingInternet/Reference/SLComposeViewController_Class/Reference/Reference.html#//apple_ref/doc/uid/TP40012205
            [controller presentViewController:mySLComposerSheet animated:YES completion:nil];
        }else{
            [AppSharer postOnFacebookURL:@""  withName:selectedTopic.topicTitle caption:@"" description:@"" andImageURL:nil completionHandler:^(BOOL success, NSError *error) {
            }];
            
        }
    }else{
        [AppSharer postOnFacebookURL:@""   withName:selectedTopic.topicTitle caption:@"" description:@"" andImageURL:nil completionHandler:^(BOOL success, NSError *error) {
            
        }];
    }
}


#pragma -mark twitter share 

-(BOOL)isTwitterAvailable {
    return NSClassFromString(@"TWTweetComposeViewController") != nil;
}

-(BOOL)isSocialAvailable {
    return NSClassFromString(@"SLComposeViewController") != nil;
}


-(void)shareAtTwitterWithTopic:(TopicModel*)selectedTopic inViewController:(UIViewController*)controller {
   
    if ([self isSocialAvailable]) {
        NSLog(@"ert");
        
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            NSString* tweetStr=selectedTopic.topicTitle.length>140?[selectedTopic.topicTitle substringToIndex:140]: selectedTopic.topicTitle;
            NSLog(@"ios 6 twitter string : %@ ",tweetStr);
            
            [tweetSheet setInitialText:tweetStr];
            
            //     [tweetSheet addImage:nil];//set the image to share with
            ///   [tweetSheet setInitialText:@"عرض علي هذا المنتج"];
            [controller presentViewController:tweetSheet animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"عفوا"
                                      message:@"لا يمكن ارسال التغريدة حاليا,من فضلك تأكد من الاتصال بالانترنت و وجود حساب تويتر معرف على الاقل "
                                      delegate:self
                                      cancelButtonTitle:@"تم"
                                      otherButtonTitles:nil];
            [alertView show];
        }
        
    } else{
        TwitterShare *twitter = [[TwitterShare alloc] owner:controller];
        [twitter TweetWithImageString:nil Link:nil Text:selectedTopic.topicTitle.length>140?[selectedTopic.topicTitle substringToIndex:140]: selectedTopic.topicTitle];
    }
    
}


@end
