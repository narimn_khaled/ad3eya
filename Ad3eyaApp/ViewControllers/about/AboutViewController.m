//
//  AboutViewController.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/22/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "AboutViewController.h"
#import "SettingViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    nibNameOrNil=DEVICE_IS_PHONE? @"AboutViewController":@"AboutViewController_iPad";
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(orientationDidCahnged:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    [self.headerIndexButton setHidden:YES];
    [self.headerBackButton setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)orientationDidCahnged:(NSNotification*)notif{
   UIDeviceOrientation ori= [notif.object orientation];
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation]; 
   
    if ( ori==UIDeviceOrientationFaceDown || ori==UIDeviceOrientationFaceUp || ori==UIDeviceOrientationPortraitUpsideDown) {
        return;
    }else{
    if (DEVICE_IS_PHONE) {
        if(ori == UIDeviceOrientationPortrait || ori==UIDeviceOrientationUnknown ){
            [self.logoButton setFrame:CGRectMake(63,281, self.logoButton.frame.size.width, self.logoButton.frame.size.height)];
        }else{
            [self.logoButton setFrame:CGRectMake(DEVICE_IS_IPHONE5?250:200,DEVICE_IS_IPHONE5? 195:195 , self.logoButton.frame.size.width, self.logoButton.frame.size.height)];
        }
    }else{
        if (ori == UIDeviceOrientationPortrait || ori==UIDeviceOrientationUnknown) {
            [self.logoButton setFrame:CGRectMake(289,511 , self.logoButton.frame.size.width, self.logoButton.frame.size.height)];
        }else{
            [self.logoButton setFrame:CGRectMake(453,462  , self.logoButton.frame.size.width, self.logoButton.frame.size.height)];
        }
    }
    }
    NSLog(@"current orientation :%d  and device orienation : %d   and logo button x : %f , y : %f ",orientation , ori, self.logoButton.frame.origin.x , self.logoButton.frame.origin.y);
}
#pragma -mark helper functions
-(void)CheckAndAdjustNightMode{
    if(nightMode){
        [self.aboutTextView setTextColor:[UIColor whiteColor]];
    }else{
        [self.aboutTextView setTextColor:[UIColor blackColor]];
    }
}


#pragma -mark 
- (IBAction)settingButtonTouched:(UIButton *)sender {
    [self presentModalViewController:[SettingViewController new] animated:YES];
}

- (IBAction)logoButtonTouched:(UIButton *)sender {
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.it4ds.com.eg"]];
}
- (void)viewDidUnload {
    [self setLogoButton:nil];
    //[self setOrientationTestView:nil];
    [self setAboutTextView:nil];
    [super viewDidUnload];
}
@end
