//
//  AboutViewController.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/22/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "masterViewController.h"

@interface AboutViewController : masterViewController


@property (weak, nonatomic) IBOutlet UITextView *aboutTextView;

@property (weak, nonatomic) IBOutlet UIButton *logoButton;
@property (weak, nonatomic) IBOutlet UITextView *orientationTestView;

- (IBAction)settingButtonTouched:(UIButton *)sender;
- (IBAction)logoButtonTouched:(UIButton *)sender;

@end
