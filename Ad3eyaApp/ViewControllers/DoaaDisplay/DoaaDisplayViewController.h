//
//  DoaaDisplayViewController.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 9/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "masterViewController.h"

#import "TTScrollSlidingPagesController.h"
#import "TTSlidingPage.h"
#import "TTSlidingPageTitle.h"

@interface DoaaDisplayViewController : masterViewController<TTSlidingPagesDataSource>
@property (nonatomic) int TopicsType;

@property (strong, nonatomic) TTScrollSlidingPagesController *slider;

@property (weak, nonatomic) IBOutlet UITableView *TopicsIndexTableView;

@end
