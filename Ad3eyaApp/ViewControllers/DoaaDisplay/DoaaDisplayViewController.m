//
//  DoaaDisplayViewController.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 9/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "DoaaDisplayViewController.h"
#import "TopicDetailsViewController.h"
#import "IndexCell.h"

@interface DoaaDisplayViewController ()<TopicDetailsViewControllerDelegate>
{
    NSMutableArray*topicsArray;
}
@end

@implementation DoaaDisplayViewController
@synthesize TopicsType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    nibNameOrNil=DEVICE_IS_PHONE?@"DoaaDisplayViewController":@"DoaaDisplayViewController_iPad";
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(orientationDidCahnged:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    topicsArray =[[appDataManger SharedInstence] getTopicsOfCategoryID:self.TopicsType];
    NSLog(@"topics array count : %i " , topicsArray.count);
    
    // Do any additional setup after loading the view from its nib.
    //initial setup of the TTScrollSlidingPagesController.
    TTScrollSlidingPagesController *slider = [[TTScrollSlidingPagesController alloc] init];
    
    //set properties to customiser the slider. Make sure you set these BEFORE you access any other properties on the slider, such as the view or the datasource. Best to do it immediately after calling the init method.
    slider.titleScrollerHidden = YES;
    //slider.titleScrollerHeight = 100;
    //slider.titleScrollerItemWidth=60;
    //slider.titleScrollerBackgroundColour = [UIColor darkGrayColor];
    //slider.disableTitleScrollerShadow = YES;
    slider.disableUIPageControl = YES;
    slider.initialPageNumber = 0;
    slider.pagingEnabled = YES;
    slider.zoomOutAnimationDisabled = YES;
    
    //set the datasource.
    slider.dataSource = self;
    
    //add the slider's view to this view as a subview, and add the viewcontroller to this viewcontrollers child collection (so that it gets retained and stays in memory! And gets all relevant events in the view controller lifecycle)
    slider.view.frame = self.view.bounds;
    [self.view addSubview:slider.view];
    [self addChildViewController:slider];
    slider.view.backgroundColor = [UIColor blackColor];
    self.slider = slider;
    
    [self.view bringSubviewToFront:self.TopicsIndexTableView];
}

-(void)viewWillAppear:(BOOL)animated{
    
    // [self.slider reloadPages];// reloadData];
      [super viewWillAppear:animated];
    [self.view bringSubviewToFront:self.TopicsIndexTableView];
    [self orientationDidCahnged:nil];
    [self.headerHomeButton setSelected:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidUnload{
    [self setTopicsIndexTableView:nil];
    self.slider=nil;
}



-(void)orientationDidCahnged:(NSNotification*)object{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if (orientation != UIInterfaceOrientationPortrait) {
        if (self.TopicsIndexTableView.frame.origin.x>100) {
            [self.TopicsIndexTableView setFrame:CGRectMake(
                                                           self.view.frame.size.width,
                                                           self.TopicsIndexTableView.frame.origin.y,
                                                        self.TopicsIndexTableView.frame.size.width,
                                                           self.TopicsIndexTableView.frame.size.height)];
        }
    }
}

#pragma -mark helper functions
-(void)CheckAndAdjustNightMode{
    if(nightMode){
        [self.TopicsIndexTableView setBackgroundColor:[UIColor blackColor]];
    }else{
        [self.TopicsIndexTableView setBackgroundColor:[UIColor whiteColor]];
    }
}

#pragma mark TTSlidingPagesDataSource methods
-(int)numberOfPagesForSlidingPagesViewController:(TTScrollSlidingPagesController *)source{
    NSLog(@"topics count : %i " , topicsArray.count);
    return topicsArray.count; //just return 7 pages as an example
}

-(TTSlidingPage *)pageForSlidingPagesViewController:(TTScrollSlidingPagesController*)source atIndex:(int)index{
    /* UIView *view;
     NSString *name = [NSString stringWithFormat:@"%i", 4 - index + 1];
     UIImage *image = [UIImage imageNamed:@""];
     if (index == 0) {
     UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
     [btn setImage:image forState:UIControlStateNormal];
     [btn addTarget:self action:@selector(dismissOnBoardingView:) forControlEvents:UIControlEventTouchUpInside];
     btn.frame = CGRectMake(0, 300, 320, 400);
     view = btn;
     }
     else{
     UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
     view = imageView;
     }*/
    // NSLog(@"index:%i",index);
    TopicDetailsViewController* topic=[[TopicDetailsViewController alloc]initWithTopic:[topicsArray objectAtIndex:index] topicNumber:index of:[topicsArray count]];
    //[topic setCurrentTopic:[topicsArray objectAtIndex:index]];
    //[topic setHidesBottomBarWhenPushed:YES];
    [topic setDelegate:self];
    return [[TTSlidingPage alloc]initWithContentViewController:topic];//[[TTSlidingPage alloc] initWithContentView:view];
}

-(TTSlidingPageTitle *)titleForSlidingPagesViewController:(TTScrollSlidingPagesController *)source atIndex:(int)index
{
    return [[TTSlidingPageTitle alloc] initWithHeaderText:@"title"];
}

-(void)dismissOnBoardingView:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        self.slider.view.alpha = 0.3;
    } completion:^(BOOL finished) {
        
        [self.slider.view removeFromSuperview];
        [self.slider removeFromParentViewController];
    }];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Onboarding"];
}

#pragma -mark topicdetails delegate
-(void)TopicViewDetailsViewControllerDidRequestShowIndex{
    if (self.TopicsIndexTableView.frame.origin.x==0) {
        [UIView animateWithDuration:.5 animations:^{
            [self.TopicsIndexTableView setFrame:CGRectMake(self.view.frame.size.width, self.TopicsIndexTableView.frame.origin.y, self.TopicsIndexTableView.frame.size.width, self.TopicsIndexTableView.frame.size.height)];
        }];
    }else{
        [UIView animateWithDuration:.5 animations:^{
            [self.TopicsIndexTableView setFrame:CGRectMake(0, self.TopicsIndexTableView.frame.origin.y, self.TopicsIndexTableView.frame.size.width, self.TopicsIndexTableView.frame.size.height)];
        }];
    }
    
}


#pragma -mark TableView delegate and Datasource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return DEVICE_IS_PHONE?70:80;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [topicsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IndexCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=(IndexCell*) [[NSBundle mainBundle]loadNibNamed:DEVICE_IS_PHONE? @"IndexCell":@"IndexCell_iPad" owner:self options:nil][0];
    }
    
    
    [cell setCellWithTopic:[topicsArray objectAtIndex:indexPath.row] inIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.slider scrollToPage:indexPath.row animated:YES];
    [UIView animateWithDuration:.5 animations:^{
        [self.TopicsIndexTableView setFrame:CGRectMake(self.view.frame.size.width, self.TopicsIndexTableView.frame.origin.y, self.TopicsIndexTableView.frame.size.width, self.TopicsIndexTableView.frame.size.height)];
    }];
}



@end
