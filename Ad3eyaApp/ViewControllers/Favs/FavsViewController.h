//
//  FavsViewController.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/22/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "masterViewController.h"

@interface FavsViewController : masterViewController <UITableViewDataSource , UITableViewDelegate>
{
    NSMutableArray * favsMArray;
}

@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;
@property (weak, nonatomic) IBOutlet UITableView *favsTableView;

- (IBAction)settingButtonTouched:(UIButton *)sender;
@end
