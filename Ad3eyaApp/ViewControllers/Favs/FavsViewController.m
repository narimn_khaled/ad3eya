//
//  FavsViewController.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/22/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "FavsViewController.h"
#import "TopicsCell.h"
#import "TopicDetailsViewController.h"
#import "DatePickerView.h"
#import "SettingViewController.h"

@interface FavsViewController ()<DatePickerViewDelegate>
{
    DatePickerView*pickerView;
}
@end

@implementation FavsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    nibNameOrNil=DEVICE_IS_PHONE?@"FavsViewController":@"FavsViewController_iPad";
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.headerBackButton setHidden:YES];
    [self.headerIndexButton setHidden:YES];
    
}

-(void)viewWillAppear:(BOOL)animated{
      [super viewWillAppear:animated];
    [self getTopicsData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFavsTableView:nil];
    [self setNoDataLabel:nil];
    [super viewDidUnload];
}
#pragma -mark helper functions
-(void)CheckAndAdjustNightMode{
    if(nightMode){
        [self.noDataLabel setTextColor:[UIColor whiteColor]];
    }else{
        [self.noDataLabel setTextColor:[UIColor blackColor]];
    }
}


- (IBAction)settingButtonTouched:(UIButton *)sender {
    [self presentModalViewController:[SettingViewController new] animated:YES];
}

-(void)getTopicsData{
    favsMArray=[[appDataManger SharedInstence]getFavoriteTopicsData];//[[NSMutableArray alloc]initWithObjects:[TopicModel new], [TopicModel new]  , [TopicModel new], nil];
    if ([favsMArray count]<=0) {
        [self.noDataLabel setHidden:NO];
        [self.favsTableView setHidden:YES];
    }else{
        [self.noDataLabel setHidden:YES];
        [self.favsTableView setHidden:NO];
        [self.favsTableView reloadData];
    }
}

#pragma -mark TableView delegate and Datasource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return DEVICE_IS_PHONE?100:150;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [favsMArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TopicsCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=(TopicsCell*) [[NSBundle mainBundle]loadNibNamed:DEVICE_IS_PHONE? @"TopicsCell":@"TopicsCell_iPad" owner:self options:nil][0];
    }
    [cell setCellWithTopic:[favsMArray objectAtIndex:indexPath.row] inIndex:indexPath.row];
    
    [cell.favButton addTarget:self action:@selector(favoriteButtonTouhced:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.alarmButton addTarget:self action:@selector(alarmButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.shareButton addTarget:self action:@selector(shareButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TopicDetailsViewController * details=[TopicDetailsViewController new];
    [details setCurrentTopic:[favsMArray objectAtIndex:indexPath.row]];
    [details setIsFromFav:YES];
    [self.navigationController pushViewController:details animated:YES];
}


#pragma -mark table Cell functions
-(IBAction)favoriteButtonTouhced:(UIButton*)sender{
    //fav button Touched
    TopicModel* topicToFav=[favsMArray objectAtIndex:sender.tag];
    [[appDataManger SharedInstence]AddRemoveTopicToFavorites:topicToFav];
    [self getTopicsData];
}

-(IBAction)alarmButtonTouched:(UIButton*)sender{
    if (!pickerView) {
          NSString *nibname=[DatePickerView getNibName];
        
      /*  UIInterfaceOrientation ori=[[UIApplication sharedApplication]statusBarOrientation];
        pickerView=[[NSBundle mainBundle]loadNibNamed: DEVICE_IS_PHONE? (ori==UIInterfaceOrientationPortrait? @"DatePickerView":@"DatePickerView_lnd"):((ori==UIInterfaceOrientationPortrait || ori==UIInterfaceOrientationPortraitUpsideDown)?@"DatePickerView_iPad":@"DatePickerView_iPad_lnd") owner:self options:nil][0];*/
         pickerView=[[NSBundle mainBundle]loadNibNamed:nibname owner:self options:nil  ][0];
        [self.view addSubview:pickerView];
        [pickerView setDelegate:self];
        [pickerView showForTopic:[favsMArray objectAtIndex:sender.tag]];
    }
}

-(IBAction)shareButtonTouched:(UIButton*)sender{
    TopicModel* topicToShare=[favsMArray objectAtIndex:sender.tag];
    [[appDataManger SharedInstence]shareTopic:topicToShare inViewController:self];//facebookShareForTopic:topicToShare inViewController:self];
}



#pragma -mark datepicker delegate
-(void)DatePickerNewAlarmSettingFinished:(BOOL)succeeded{
    if (succeeded) {
        [self getTopicsData];
    }
    pickerView=nil;
}


@end
