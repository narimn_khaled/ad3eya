//
//  IndexCell.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 9/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "IndexCell.h"

@implementation IndexCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected) {
        bgImageName=[NSString stringWithFormat:(DEVICE_IS_PHONE? @"%i_select":@"%i_select_lndscp"),selectionIndex];
    }else{
        if (cellIndex%2) {
            bgImageName= DEVICE_IS_PHONE ? @"list_item_g" : @"list_item_g_lndscp";
        }else{
            bgImageName= DEVICE_IS_PHONE? @"list_item_w":@"list_item_w_lndscp";
        }
    }
    bgImageName=[self getAdjustedBgImageName:bgImageName];
    [self.bgImage setImage:[UIImage imageNamed:bgImageName]];
       // Configure the view for the selected state
}

-(void)setCellWithTopic:(TopicModel *)topic inIndex:(int)index{
    cellIndex =index;
   
    [self.bgImage setImage:[UIImage imageNamed:bgImageName]];
    selectionIndex=[topic.categoryOrder integerValue];
    
    if(nightMode) {
        [self.topicLabel setTextColor:[UIColor whiteColor]];
        [self.topicIndexLbl setTextColor:[UIColor whiteColor]];
    }else{
        [self.topicLabel setTextColor:[UIColor blackColor]];
        [self.topicIndexLbl setTextColor:[UIColor blackColor]];
    }
    [self.topicLabel setText:topic.topicTitle];
    [self.topicIndexLbl setText:[NSString stringWithFormat:@"%i",index+1]];
    
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(orientationDidCahnged:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}
-(void)orientationDidCahnged:(NSNotification*)object{
    [self.bgImage setImage:[UIImage imageNamed:[self getAdjustedBgImageName:bgImageName]]];
}

-(NSString *)getAdjustedBgImageName:(NSString *) img{
    if (DEVICE_IS_PHONE) {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        if (orientation == UIInterfaceOrientationPortrait || orientation ==UIInterfaceOrientationPortraitUpsideDown) {
            if ([img rangeOfString:@"_lndscp"].location!=NSNotFound) {
                img= [img substringToIndex:[img rangeOfString:@"_lndscp"].location  ];
            }
        }else{
            if ([img rangeOfString:@"_lndscp"].location==NSNotFound) {
                img=[NSString stringWithFormat:@"%@_lndscp",img];
            }
        }
    }
    else{
        if ([img rangeOfString:@"_lndscp"].location==NSNotFound) {
            img=[NSString stringWithFormat:@"%@_lndscp",img];
        }
    }
    return img;
}
@end
