//
//  IndexCell.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 9/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopicModel.h"

@interface IndexCell : UITableViewCell
{
    int cellIndex;
    int selectionIndex;
    NSString * bgImageName;
}
@property (weak, nonatomic) IBOutlet UILabel *topicLabel;
@property (weak, nonatomic) IBOutlet UILabel *topicIndexLbl;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

-(void)setCellWithTopic:(TopicModel*) topic inIndex:(int)index;

@end
