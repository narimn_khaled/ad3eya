//
//  TopicsCell.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "TopicsCell.h"

@implementation TopicsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected) {
        [self.bgImage setImage:[UIImage imageNamed:@"common_list_item_s1@2x.png"]];
    }else{
        [self.bgImage setImage:[UIImage imageNamed:@"common_list_item_s0@2x.png"]];
    }
    // Configure the view for the selected state
}

-(void)setCellWithTopic:(TopicModel *)topic inIndex:(int)index{
    nightMode ? [self.topicTitle setTextColor:[UIColor whiteColor]] :[self.topicTitle setTextColor:[UIColor blackColor]];
    NSLog(@"cell night mode : %@ " , nightMode?@"yes" :@"NO") ;
    [self.topicTitle setText:topic.topicTitle];
    
    [self.favButton setTag:index];
    [self.shareButton setTag:index] ;
    [self.alarmButton setTag:index];
    if (topic.isFav==YES) {
        [self.favButton setImage:[UIImage imageNamed:@"common_del_s0@2x"] forState:UIControlStateNormal];
        [self.favButton setImage:[UIImage imageNamed:@"common_del_s1@2x"] forState:UIControlStateHighlighted];
    }else{
        [self.favButton setImage:[UIImage imageNamed:@"common_fav_s0@2x"] forState:UIControlStateNormal];
        [self.favButton setImage:[UIImage imageNamed:@"common_fav_s1@2x"] forState:UIControlStateHighlighted];

    }
    
    
    if (topic.haveAlarm) {
        [self.alarmButton setImage:[UIImage imageNamed:@"common_edit_s0@2x"] forState:UIControlStateNormal];
        [self.alarmButton setImage:[UIImage imageNamed:@"common_edit_s1@2x"] forState:UIControlStateHighlighted];
    }else{
        [self.alarmButton setImage:[UIImage imageNamed:@"common_alarm_s0@2x"] forState:UIControlStateNormal];
        [self.alarmButton setImage:[UIImage imageNamed:@"common_alarm_s1@2x"] forState:UIControlStateHighlighted];
        
    }
    
}

@end
