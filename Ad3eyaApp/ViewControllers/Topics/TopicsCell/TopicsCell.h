//
//  TopicsCell.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopicModel.h"

@interface TopicsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

@property (weak, nonatomic) IBOutlet UILabel *topicTitle;

@property (weak, nonatomic) IBOutlet UIButton *favButton;
@property (weak, nonatomic) IBOutlet UIButton *alarmButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

-(void)setCellWithTopic:(TopicModel*)topic inIndex:(int)index;

@end
