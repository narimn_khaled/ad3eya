//
//  TopicsViewController.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/23/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "TopicsViewController.h"
#import "TopicsCell.h"
#import "TopicDetailsViewController.h"
#import "TopicModel.h"
#import "DatePickerView.h"
#import "FTAnimation+UIView.h"

#import "appDataManger.h"
#import "SettingViewController.h"

@interface TopicsViewController ()<DatePickerViewDelegate , UIActionSheetDelegate > {
    NSMutableArray * topicsArray;
    DatePickerView * pickerView;
    TopicModel* toBeSharedTopic;
}
@end

@implementation TopicsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    nibNameOrNil= DEVICE_IS_PHONE ? @"TopicsViewController":@"TopicsViewController_iPad";
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.pageTitle setText:[[[[appDataManger SharedInstence] Categories]objectForKey:[NSString stringWithFormat:@"%i",self.TopicsType]] objectForKey:@"name"]];
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
      [super viewWillAppear:animated];
      [self getTopicsData];
}

- (void)viewDidUnload {
    [self setPageTitle:nil];
    [self setTopicsTableView:nil];
    [super viewDidUnload];
}


-(NSString* )getCategoryName{
    switch (self.TopicsType) {
        case 1:
            return  @"أذكار الصلاة";
            break;
            
        case 2:
            return @"أذكار المساء";
            break;
            
        case 3:
            return @"أذكار الصباح";
            break;
            
        case 4:
            return @"من دعاء الرسول";
            break;
            
        case 5:
            return @"أدعية من القرآن";
            break;
            
        case 6:
            return @"أذكار النوم";
            break;
            
        case 7:
            return @"أذكار متفرقة";
            break;
            
        case 8:
            return @"الرقية بالسنة";
            break;
        case 9:
            return @"الرقية بالقرآن";
            break;
    }
    return @"";
}

-(void)getTopicsData{
    /* topicsArray=[[NSMutableArray alloc]initWithObjects:[[TopicModel alloc]initWithTitle:@"title 1" category:self.pageTitle.text andFav:NO], [[TopicModel alloc] initWithTitle:@"title 2 " category:self.pageTitle.text andFav:YES]  , [TopicModel new], nil];*/
    topicsArray =[[appDataManger SharedInstence] getTopicsOfCategoryID:self.TopicsType];
    [self.topicsTableView reloadData];
}

#pragma -mark IBActions
- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)settingButtonTouched:(UIButton *)sender {
    [self presentModalViewController:[SettingViewController new] animated:YES];
}

#pragma -mark TableView delegate and Datasource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return DEVICE_IS_PHONE?100:150;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [topicsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TopicsCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=(TopicsCell*) [[NSBundle mainBundle]loadNibNamed:DEVICE_IS_PHONE? @"TopicsCell":@"TopicsCell_iPad" owner:self options:nil][0];
    }
    
    
    [cell setCellWithTopic:[topicsArray objectAtIndex:indexPath.row] inIndex:indexPath.row];
    
    [cell.favButton addTarget:self action:@selector(favoriteButtonTouhced:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.alarmButton addTarget:self action:@selector(alarmButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.shareButton addTarget:self action:@selector(shareButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TopicDetailsViewController * details=[TopicDetailsViewController new];
    [details setCurrentTopic:[topicsArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:details animated:YES];
}



#pragma -mark table Cell functions
-(IBAction)favoriteButtonTouhced:(UIButton*)sender{
    //fav button Touched
    TopicModel* topicToFav=[topicsArray objectAtIndex:sender.tag];
    [[appDataManger SharedInstence]AddRemoveTopicToFavorites:topicToFav];
    [self getTopicsData];
}

-(IBAction)alarmButtonTouched:(UIButton*)sender{
    if (!pickerView) {
        pickerView=[[NSBundle mainBundle]loadNibNamed: DEVICE_IS_PHONE? @"DatePickerView":@"DatePickerView_iPad" owner:self options:nil][0];
        [self.view addSubview:pickerView];
        [pickerView setDelegate:self];
        [pickerView showForTopic:[topicsArray objectAtIndex:sender.tag]];
    }
}

-(IBAction)shareButtonTouched:(UIButton*)sender{
    [[appDataManger SharedInstence]shareTopic:[topicsArray objectAtIndex:sender.tag] inViewController:self];
    /*
    toBeSharedTopic=[topicsArray objectAtIndex:sender.tag];
    UIActionSheet *sheet=[[UIActionSheet alloc]initWithTitle:@"نشر على" delegate:self cancelButtonTitle:@"إلغاء" destructiveButtonTitle:nil otherButtonTitles:@"فيس بوك",@"تويتر", nil];
    [sheet showFromTabBar:tabBar.tabBar ];//  showInView:self.view];
    //
     */
}

-(void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [[appDataManger SharedInstence]facebookShareForTopic:toBeSharedTopic inViewController:self];
            break;
        case 1:
            [[appDataManger SharedInstence]shareAtTwitterWithTopic:toBeSharedTopic inViewController:self];
            break;
        default:
            break;
    }
toBeSharedTopic=nil;
}

#pragma -mark datepicker delegate
-(void)DatePickerNewAlarmSettingFinished:(BOOL)succeeded{
    if (succeeded) {
        [self getTopicsData];
    }
    pickerView=nil;
}

@end
