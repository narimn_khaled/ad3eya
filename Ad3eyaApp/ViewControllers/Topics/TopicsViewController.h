//
//  TopicsViewController.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/23/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopicsViewController : UIViewController <UITableViewDataSource , UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UITableView *topicsTableView;

@property (nonatomic) int TopicsType;
- (IBAction)goBack:(id)sender;
- (IBAction)settingButtonTouched:(UIButton *)sender;

@end
