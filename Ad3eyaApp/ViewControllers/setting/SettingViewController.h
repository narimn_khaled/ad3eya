//
//  SettingViewController.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 9/1/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "masterViewController.h"

@interface SettingViewController : masterViewController


@property (weak, nonatomic) IBOutlet UILabel *fontSizeLabel;
@property (weak, nonatomic) IBOutlet UITextField *fontSizeText;

@property (weak, nonatomic) IBOutlet UILabel *nightModeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *nightModeSwitch;

- (IBAction)nightModeSwitchValueChanged:(UISwitch *)sender;

- (IBAction)fontTextValueChanged:(UITextField *)sender;

- (IBAction)backgroundTouched:(UIControl *)sender;
- (IBAction)increaseFontButtonTouched:(UIButton *)sender;
- (IBAction)decreaseFontButtonTouched:(UIButton *)sender;

- (IBAction)saveSettingButtonTouched:(UIButton *)sender;

- (IBAction)backButtonTouched:(UIButton *)sender;

@end
