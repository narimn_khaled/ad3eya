//
//  SettingViewController.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 9/1/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "SettingViewController.h"
#define biggerError  @"برجاء ادخال قيمة اقل من %@"
@interface SettingViewController ()

@end

@implementation SettingViewController
-(id)init{
    return [self initWithNibName:@"" bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    nibNameOrNil=DEVICE_IS_PHONE?@"SettingViewController":@"SettingViewController_iPad";
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.headerBackButton setHidden:YES];
    [self.headerIndexButton setHidden:YES];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSString* fontSize=[[NSUserDefaults standardUserDefaults]objectForKey:@"setting"] ;
    [self.fontSizeText setText: fontSize?fontSize:(DEVICE_IS_PHONE ? @"16":@"22")];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nightModeSwitchValueChanged:(UISwitch *)sender {
    NSLog(@"switch value : %@" ,[sender isOn]?@"on" :@"off");
        [self AdjustNightModeWithMode:[sender isOn]];
    
}
#pragma -mark helper functions
-(void)AdjustNightModeWithMode:(BOOL)Mode{
    if(Mode){
        [self.view setBackgroundColor:[UIColor blackColor]];
        [self.fontSizeLabel setTextColor:[UIColor whiteColor]];
        [self.nightModeLabel setTextColor:[UIColor whiteColor]];
        [self.fontSizeText setTextColor:[UIColor whiteColor]];
    }else{
        [self .view setBackgroundColor:[UIColor whiteColor]];
        [self.fontSizeLabel setTextColor:[UIColor blackColor]];
        [self.nightModeLabel setTextColor:[UIColor blackColor]];
        [self.fontSizeText setTextColor:[UIColor blackColor]];
    }
}

-(void)CheckAndAdjustNightMode{
    [self.nightModeSwitch setOn:nightMode];
    if(nightMode){
        [self.view setBackgroundColor:[UIColor blackColor]];
        [self.fontSizeLabel setTextColor:[UIColor whiteColor]];
        [self.nightModeLabel setTextColor:[UIColor whiteColor]];
        [self.fontSizeText setTextColor:[UIColor whiteColor]];
    }else{
        [self .view setBackgroundColor:[UIColor whiteColor]];
        [self.fontSizeLabel setTextColor:[UIColor blackColor]];
        [self.nightModeLabel setTextColor:[UIColor blackColor]];
        [self.fontSizeText setTextColor:[UIColor blackColor]];
    }
}
- (IBAction)fontTextValueChanged:(UITextField *)sender {

        
}

- (IBAction)backgroundTouched:(UIControl *)sender {
    [self.fontSizeText resignFirstResponder];
    if (![self.fontSizeText.text integerValue]) {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"تنبيه" message:@"من فضلك قم بادخال قيمة صحيحة" delegate:nil cancelButtonTitle:@"تم" otherButtonTitles: nil];
        [alert show];
    }else if ([self.fontSizeText.text integerValue]> (DEVICE_IS_PHONE?50:70)){
        NSString *errorMsg=[NSString stringWithFormat:@"القيمة المدخلة كبيرة,برجاء ادخال قيمة اقل من %@",(DEVICE_IS_PHONE?@"٥٠":@"٧٠")];
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"تنبيه" message:errorMsg delegate:nil cancelButtonTitle:@"تم" otherButtonTitles: nil];
        [alert show];
    }else if ([self.fontSizeText.text integerValue]< (DEVICE_IS_PHONE?5:10)){
           NSString *errorMsg=[NSString stringWithFormat:@"القيمة المدخلة صغيرة,برجاء ادخال قيمة اكبر من %@",(DEVICE_IS_PHONE?@"٥":@"١٠")];
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"تنبيه" message:errorMsg delegate:nil cancelButtonTitle:@"تم" otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)increaseFontButtonTouched:(UIButton *)sender {
    [self.fontSizeText setText:[NSString stringWithFormat:@"%i", [self.fontSizeText.text integerValue]+1]];
}

- (IBAction)decreaseFontButtonTouched:(UIButton *)sender {
     [self.fontSizeText setText:[NSString stringWithFormat:@"%i", [self.fontSizeText.text integerValue]-1]];
}

- (IBAction)saveSettingButtonTouched:(UIButton *)sender {
    if (![self.fontSizeText.text integerValue]) {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"تنبيه" message:@"من فضلك قم بادخال قيمة صحيحة" delegate:nil cancelButtonTitle:@"تم" otherButtonTitles: nil];
        [alert show];
    }else if ([self.fontSizeText.text integerValue]> (DEVICE_IS_PHONE?50:70)){
           NSString *errorMsg=[NSString stringWithFormat:@"القيمة المدخلة كبيرة,برجاء ادخال قيمة اقل من %@",(DEVICE_IS_PHONE?@"٥٠":@"٧٠")];
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"تنبيه" message:errorMsg delegate:nil cancelButtonTitle:@"تم" otherButtonTitles: nil];
        [alert show];
    }else if ([self.fontSizeText.text integerValue]< (DEVICE_IS_PHONE?5:10)){
         NSString *errorMsg=[NSString stringWithFormat:@"القيمة المدخلة صغيرة,برجاء ادخال قيمة اكبر من %@",(DEVICE_IS_PHONE?@"٥":@"١٠")];
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"تنبيه" message:errorMsg delegate:nil cancelButtonTitle:@"تم" otherButtonTitles: nil];
        [alert show];
    }else{
        [[NSUserDefaults standardUserDefaults]setObject:self.fontSizeText.text forKey:@"setting"];
        [[NSUserDefaults standardUserDefaults ]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setBool:[self.nightModeSwitch isOn] forKey:@"nightMode"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        nightMode =[self.nightModeSwitch isOn];

        
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"تنبيه" message:@"تم حفظ الاعدادات بنجاح" delegate:nil cancelButtonTitle:@"تم" otherButtonTitles: nil];
        [alert show];
        
        [self backButtonTouched:nil];
    }
}

- (IBAction)backButtonTouched:(UIButton *)sender {
    [self dismissModalViewControllerAnimated:YES];
}
- (void)viewDidUnload {
    [self setFontSizeText:nil];
    [self setNightModeSwitch:nil];
    [self setFontSizeLabel:nil];
    [self setNightModeLabel:nil];
    [super viewDidUnload];
}
@end
