//
//  AlarmsViewController.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/22/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "masterViewController.h"

@interface AlarmsViewController : masterViewController<UITableViewDataSource ,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *alarmsTableView;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

- (IBAction)settingButtonTouched:(UIButton *)sender;
@end
