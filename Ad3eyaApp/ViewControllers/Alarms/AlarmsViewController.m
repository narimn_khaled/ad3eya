//
//  AlarmsViewController.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/22/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "AlarmsViewController.h"
#import "TopicsCell.h"
#import "TopicDetailsViewController.h"
#import "DatePickerView.h"
#import "SettingViewController.h"

@interface AlarmsViewController () <DatePickerViewDelegate>
{
    NSMutableArray * alarmsArray;
    DatePickerView* datePicker;
}
@end

@implementation AlarmsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    nibNameOrNil=DEVICE_IS_PHONE?@"AlarmsViewController":@"AlarmsViewController_iPad";
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.headerBackButton setHidden:YES];
    [self.headerIndexButton setHidden:YES];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
        [self getAlarms];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setAlarmsTableView:nil];
    [self setNoDataLabel:nil];
    [super viewDidUnload];
}

#pragma -mark helper functions
-(void)CheckAndAdjustNightMode{
    if(nightMode){
        [self.noDataLabel setTextColor:[UIColor whiteColor]];
    }else{
        [self.noDataLabel setTextColor:[UIColor blackColor]];
    }
}
- (IBAction)settingButtonTouched:(UIButton *)sender {
    [self presentModalViewController:[SettingViewController new] animated:YES];
}

-(void)getAlarms{
   alarmsArray=[[appDataManger SharedInstence]getAlarmTopicsData];
  // alarmsArray=  [[[UIApplication sharedApplication] scheduledLocalNotifications] mutableCopy];
    NSLog(@"alarma array : %@",alarmsArray);
    if ([alarmsArray count]>0) {
        [self.alarmsTableView reloadData];
        [self.noDataLabel setHidden:YES];
        [self.alarmsTableView setHidden:NO];
    }else{
        [self.noDataLabel setHidden:NO];
        [self.alarmsTableView setHidden:YES];
    }
}

#pragma -mark TableView delegate and Datasource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return DEVICE_IS_PHONE?100:150;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [alarmsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TopicsCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=(TopicsCell*) [[NSBundle mainBundle]loadNibNamed:DEVICE_IS_PHONE? @"TopicsCell":@"TopicsCell_iPad" owner:self options:nil][0];
    }
    
 //   [cell setCellWithTopic:[alarmsArray objectAtIndex:indexPath.row ] inIndex:indexPath.row];
   // UILocalNotification * notif=[alarmsArray objectAtIndex:indexPath.row];
    [cell.topicTitle setText:[[alarmsArray objectAtIndex:indexPath.row] topicTitle]];// notif.alertBody];
    nightMode?[cell.topicTitle setTextColor:[UIColor whiteColor]]:[cell.topicTitle setTextColor:[UIColor blackColor]];
    [cell.favButton setSelected:YES];
    [cell.favButton setTag:indexPath.row];
    [cell.favButton setImage:[UIImage imageNamed:@"common_del_s0@2x"] forState:UIControlStateNormal];
    [cell.favButton setImage:[UIImage imageNamed:@"common_del_s1@2x"] forState:UIControlStateHighlighted];
    [cell.favButton addTarget:self action:@selector(removeAlarmButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.alarmButton setSelected:YES];
    [cell.alarmButton setTag:indexPath.row];
    [cell.alarmButton setImage:[UIImage imageNamed:@"common_edit_s0@2x"] forState:UIControlStateNormal];
    [cell.alarmButton setImage:[UIImage imageNamed:@"common_edit_s1@2x"] forState:UIControlStateHighlighted];
    [cell.alarmButton addTarget:self action:@selector(editAlarmButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.shareButton setTag:indexPath.row];
    [cell.shareButton addTarget:self action:@selector(shareButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TopicDetailsViewController * details=[TopicDetailsViewController new];
    [self.navigationController pushViewController:details animated:YES];
}




-(IBAction)removeAlarmButtonTouched:(UIButton*)sender{

    [[appDataManger SharedInstence]RemoveAlarmForTopic:[alarmsArray objectAtIndex:sender.tag]];
    
    [self getAlarms];
}

-(IBAction)editAlarmButtonTouched:(UIButton*)sender{
    if (!datePicker) {
         NSString *nibname=[DatePickerView getNibName];
       /* UIInterfaceOrientation ori=[[UIApplication sharedApplication]statusBarOrientation];
        datePicker=[[NSBundle mainBundle]loadNibNamed: DEVICE_IS_PHONE? (ori==UIInterfaceOrientationPortrait? @"DatePickerView":@"DatePickerView_lnd"):((ori==UIInterfaceOrientationPortrait || ori==UIInterfaceOrientationPortraitUpsideDown)?@"DatePickerView_iPad":@"DatePickerView_iPad_lnd") owner:self options:nil][0];*/
        datePicker=[[NSBundle mainBundle]loadNibNamed:nibname owner:self options:nil  ][0];
    [self.view addSubview: datePicker];
    [datePicker setDelegate:self];
    
    [datePicker showForTopic:[alarmsArray objectAtIndex:sender.tag]];
    }
}

-(IBAction)shareButtonTouched:(UIButton*)sender{
    TopicModel* topicToShare=[alarmsArray objectAtIndex:sender.tag];
    [[appDataManger SharedInstence]shareTopic:topicToShare inViewController:self];//facebookShareForTopic:topicToShare inViewController:self];
}

#pragma -mark datePicker delegate
-(void)DatePickerNewAlarmSettingFinished:(BOOL)succeeded{
    if (succeeded) {
         [self getAlarms];
    }
    datePicker=nil;
    
}



@end
