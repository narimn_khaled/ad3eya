//
//  ViewController.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/22/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIGridView.h"
#import "UIGridViewDelegate.h"
#import "masterViewController.h"

@interface ViewController : masterViewController <UITableViewDataSource , UITableViewDelegate ,UIGridViewDelegate >
{
    
}

@property (weak, nonatomic) IBOutlet UITableView *categoryTable;
@property (weak, nonatomic) IBOutlet UIGridView *categoryGrid;


- (IBAction)MenuButtonTouched:(UIButton *)sender;

- (IBAction)settingButtonTouched:(UIButton *)sender;

@end
