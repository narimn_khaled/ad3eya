//
//  MenuGridCell.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/29/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MenuGridCell.h"

@implementation MenuGridCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)setCellWithCategoryDic:(NSDictionary *)Dic{
  //  [self.categoryName setText:[Dic objectForKey:@"name"]];
    [self.categoryImage setImage:[UIImage imageNamed:[Dic objectForKey:@"order"]]];
}
@end
