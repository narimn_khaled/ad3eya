//
//  MenuGridCell.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/29/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "UIGridViewCell.h"

@interface MenuGridCell : UIGridViewCell
@property (weak, nonatomic) IBOutlet UILabel *categoryName;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImage;



-(void)setCellWithCategoryDic:(NSDictionary*)Dic;
@end
