//
//  ViewController.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/22/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "ViewController.h"
#import "TopicsViewController.h"
#import "MenuCell.h"
#import "MenuGridCell.h"
#import "SettingViewController.h"
#import "DoaaDisplayViewController.h"
#import "SVProgressHUD.h"

@interface ViewController ()
{
    int numberOfGridColoms;
}
@end

@implementation ViewController
-(id)init{
    return [self initWithNibName:@"" bundle:nil];
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    nibNameOrNil=DEVICE_IS_PHONE?@"ViewController":@"ViewController_iPad";
    return [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(orientationDidCahnged:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    [self.headerBackButton setHidden:YES];
    [self.headerIndexButton setHidden:YES];
    [self.headerHomeButton setSelected:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
}
- (void)viewDidUnload {
    [self setCategoryTable:nil];
    [self setCategoryGrid:nil];
    
    [super viewDidUnload];
}


- (IBAction)MenuButtonTouched:(UIButton *)sender {
    TopicsViewController * topics=[TopicsViewController new];
    [topics setTopicsType:sender.tag];
    [self.navigationController pushViewController:topics animated:YES];
}

- (IBAction)settingButtonTouched:(UIButton *)sender {
    [self presentModalViewController:[SettingViewController new] animated:YES];
}


-(void)orientationDidCahnged:(NSNotification*)object{
    [self.categoryGrid reloadData];
}





#pragma -mark TableView delegate and Datasource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return DEVICE_IS_PHONE?60:90;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[appDataManger SharedInstence] CategoriesIDs] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MenuCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=(MenuCell*) [[NSBundle mainBundle]loadNibNamed:DEVICE_IS_PHONE? @"MenuCell":@"MenuCell_iPad" owner:self options:nil][0];
    }
    
    NSString * catId=[[[appDataManger SharedInstence]CategoriesIDs]objectAtIndex:indexPath.row];
    [cell.categoryName setText:[[[appDataManger SharedInstence] Categories]objectForKey:catId]];//[[appDataManger SharedInstence]_categ]  allValues]objectAtIndex:indexPath.row] objectForKey:@"name"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TopicsViewController * topics=[TopicsViewController new];
    [topics setTopicsType:[[NSString stringWithFormat:@"%@", [[[[appDataManger SharedInstence] Categories]allKeys]objectAtIndex:indexPath.row]]integerValue]];
    [self.navigationController pushViewController:topics animated:YES];
}

#pragma -mark gridView delegate and dataSource


-(CGFloat) gridView:(UIGridView *)grid heightForRowAt:(int)rowIndex{
    return DEVICE_IS_PHONE?100:200;;
}

-(CGFloat) gridView:(UIGridView *)grid widthForColumnAt:(int)columnIndex{
    return DEVICE_IS_PHONE?106:256;
}

-(NSInteger)numberOfColumnsOfGridView:(UIGridView *)grid{
    UIInterfaceOrientation  orientation=[[UIApplication sharedApplication]statusBarOrientation ];
    numberOfGridColoms=UIInterfaceOrientationIsLandscape(orientation) ?(DEVICE_IS_IPHONE5?5: 4): 3;
    return  numberOfGridColoms;
}

-(NSInteger)numberOfCellsOfGridView:(UIGridView *)grid{
    return [[[appDataManger SharedInstence] Categories] count];

}

- (UIGridViewCell *) gridView:(UIGridView *)grid cellForRowAt:(int)rowIndex AndColumnAt:(int)columnIndex
{
    MenuGridCell *cell = (MenuGridCell *)[grid dequeueReusableCell];
    
    if (!cell) {
        NSString* cellNib=@"";
        if (DEVICE_IS_PHONE) {
            cellNib=@"MenuGridCell";
        }else {
            cellNib=@"MenuGridCell_iPad";
        }
        NSArray* topArray=[[NSBundle mainBundle]loadNibNamed:cellNib owner:self options:nil];
        for (NSObject *item in topArray) {
            if([item isKindOfClass:[MenuGridCell class]])
            {
                cell=(MenuGridCell*)item;
            }
        }
    }
    columnIndex=numberOfGridColoms-columnIndex-1;
    
    NSLog(@"coloum index : %i" , columnIndex);
    
   
    
    int index=0;
    if ((numberOfGridColoms*rowIndex)+columnIndex < [[[appDataManger SharedInstence] Categories]count]) {
        index=(numberOfGridColoms*rowIndex)+columnIndex;
    }else{
        index=(numberOfGridColoms * rowIndex)+(numberOfGridColoms-(1+columnIndex));
    }
    
         NSString * catId=[[[appDataManger SharedInstence]CategoriesIDs]objectAtIndex: index];
    NSLog(@"cellFor Row -> actual index: %i ,   object index : %i and category ID : %@" ,(numberOfGridColoms*rowIndex)+columnIndex, index , catId);
    

    
    [cell setCellWithCategoryDic:[[[appDataManger SharedInstence] Categories]objectForKey:catId ]];
    return cell;
}
- (void) gridView:(UIGridView *)grid didSelectRowAt:(int)rowIndex AndColumnAt:(int)columnIndex
{
    [SVProgressHUD show];
    __block int cIndex;
    [UIView animateWithDuration:.1 animations:^{
        
    }completion:^(BOOL finished) {
        cIndex=numberOfGridColoms-columnIndex-1;
        
        int index=0;
        if ((numberOfGridColoms*rowIndex)+cIndex < [[[appDataManger SharedInstence] Categories]count]) {
            index=(numberOfGridColoms*rowIndex)+cIndex;
        }else{
            index=(numberOfGridColoms * rowIndex)+(numberOfGridColoms-(1+cIndex));
        }
      
        
        DoaaDisplayViewController * topics=[DoaaDisplayViewController new];
    
        int catID=[[NSString stringWithFormat:@"%@", [[[appDataManger SharedInstence] CategoriesIDs]objectAtIndex:index]]integerValue];
        
          NSLog(@"selected cell -> actual index: %i ,   object index : %i  and category ID : %i" ,(numberOfGridColoms*rowIndex)+columnIndex, index , catID);
        
        [topics setTopicsType:catID];
         //(numberOfGridColoms*rowIndex)+cIndex < [[[appDataManger SharedInstence] Categories]count] ? (numberOfGridColoms*rowIndex)+cIndex :[[[appDataManger SharedInstence] Categories]count]-1 ]
        [self.navigationController pushViewController:topics animated:YES];
    }];
     
}





@end
