//
//  MenuCell.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/27/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if (selected) {
        [self.bgImage setImage:[UIImage imageNamed:@"common_list_item_s1@2x.png"]];
    }else{
        [self.bgImage setImage:[UIImage imageNamed:@"common_list_item_s0@2x.png"]];
    }

}

@end
