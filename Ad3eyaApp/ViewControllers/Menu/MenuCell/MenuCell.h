//
//  MenuCell.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/27/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *categoryName;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

@end
