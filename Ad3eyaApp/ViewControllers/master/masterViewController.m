//
//  masterViewController.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 9/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "masterViewController.h"

@interface masterViewController ()

@end

@implementation masterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.headerViewImage setFrame: CGRectMake(0, 0, WINDOW_DIMENSIONS.size.width,DEVICE_IS_PHONE? 44:80)];
    [self.headerViewImage setImage:[UIImage imageNamed:@"header_bg"]];
    
    
    [self.pageTitle setTextAlignment:NSTextAlignmentCenter];
    [self.pageTitle setFrame:DEVICE_IS_PHONE? CGRectMake(80, 11, 205, 21):CGRectMake(180, 14, 513, 52)];
    [self.pageTitle setFont:[UIFont boldSystemFontOfSize:(DEVICE_IS_PHONE? 17:27)]];
    [self.pageTitle setTextColor:[UIColor whiteColor]];
    self.pageTitle.autoresizingMask = (UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin) ;
    
   // self.headerBackButton =[UIButton buttonWithType:UIButtonTypeCustom] ;
    [self.headerBackButton setBackgroundImage:[UIImage imageNamed:DEVICE_IS_PHONE? @"btn_header_back_s0": @"iPad_btn_header_back_s0"] forState:UIControlStateNormal];
    [self.headerBackButton setBackgroundImage:[UIImage imageNamed:DEVICE_IS_PHONE?@"btn_header_back_s1": @"iPad_btn_header_back_s1"] forState:UIControlStateHighlighted];
    [self.headerBackButton setFrame:DEVICE_IS_PHONE? CGRectMake(11, 4, 29, 35):CGRectMake(20, 10, 48, 60)];
    [self.headerBackButton addTarget:self action:@selector(headerBackButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    self.headerBackButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    
    //self.headerIndexButton=[UIButton buttonWithType:UIButtonTypeCustom] ;
    [self.headerIndexButton setBackgroundImage:[UIImage imageNamed:DEVICE_IS_PHONE?@"btn_header_index_s0": @"iPad_btn_header_index_s0"] forState:UIControlStateNormal];
    [self.headerIndexButton setBackgroundImage:[UIImage imageNamed:DEVICE_IS_PHONE?@"btn_header_index_s1": @"iPad_btn_header_index_s1"] forState:UIControlStateHighlighted];
    [self.headerIndexButton setFrame:DEVICE_IS_PHONE? CGRectMake(55, 6, 35, 33):CGRectMake(117, 10, 62, 58)];
    [self.headerIndexButton addTarget:self action:@selector(headerIndexButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    self.headerIndexButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    
    //self.headerHomeButton=[UIButton buttonWithType:UIButtonTypeCustom] ;
    [self.headerHomeButton setBackgroundImage:[UIImage imageNamed:DEVICE_IS_PHONE? @"btn_header_home_s0":@"iPad_btn_header_home_s0"] forState:UIControlStateNormal];
    [self.headerHomeButton setBackgroundImage:[UIImage imageNamed:DEVICE_IS_PHONE?@"btn_header_home_s1": @"iPad_btn_header_home_s1"] forState:UIControlStateHighlighted];
    [self.headerHomeButton setBackgroundImage:[UIImage imageNamed:DEVICE_IS_PHONE?@"btn_header_home_s1": @"iPad_btn_header_home_s1"] forState:UIControlStateSelected];
    [self.headerHomeButton setFrame:DEVICE_IS_PHONE? CGRectMake(280, 7, 35, 32):CGRectMake(693, 10, 66, 59)];
    [self.headerHomeButton addTarget:self action:@selector(headerHomeButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    self.headerHomeButton.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin;
  //  [self.headerHomeButton setBackgroundColor:[UIColor redColor]];
    
    
    
}



-(void)viewWillAppear:(BOOL)animated{
    [self masterCheckAndAdjustNightMode];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft)|| (interfaceOrientation == UIInterfaceOrientationLandscapeRight) || (interfaceOrientation == UIInterfaceOrientationPortrait));
}



-(void)masterCheckAndAdjustNightMode{
     BOOL Mode= [[NSUserDefaults standardUserDefaults ]boolForKey:nightModeKey ] ;;
    if( Mode )
    {
        [self.view setBackgroundColor:[UIColor blackColor]];
        nightMode =YES;
    }else{
        [self.view setBackgroundColor:[UIColor whiteColor]];
        nightMode=NO;
    }
    if ([self respondsToSelector:@selector(CheckAndAdjustNightMode)]) {
        [self CheckAndAdjustNightMode];
    }
}



#pragma -mark IBActions

-(void)headerBackButtonTouched:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)headerIndexButtonTouched:(UIButton *)sender{
    
}

-(void)headerHomeButtonTouched:(UIButton *)sender{
    [tabBar setSelectedIndex:4];
    [((UINavigationController*)[tabBar viewControllers][4]) popToRootViewControllerAnimated:YES];
}
@end
