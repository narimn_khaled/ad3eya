//
//  masterViewController.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 9/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface masterViewController : UIViewController
@property (weak , nonatomic) IBOutlet UIImageView * headerViewImage;
@property (weak , nonatomic) IBOutlet UIButton * headerHomeButton;
@property (weak , nonatomic) IBOutlet UIButton * headerIndexButton;
@property (weak , nonatomic) IBOutlet UIButton * headerBackButton;
@property (weak , nonatomic) IBOutlet UILabel * pageTitle;

-(void)masterCheckAndAdjustNightMode;
-(void)CheckAndAdjustNightMode;

-(void)headerBackButtonTouched:(UIButton *)sender;
-(void)headerIndexButtonTouched:(UIButton *)sender;
-(void)headerHomeButtonTouched:(UIButton *)sender;

@end
