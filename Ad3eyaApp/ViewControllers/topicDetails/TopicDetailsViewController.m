//
//  TopicDetailsViewController.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "TopicDetailsViewController.h"
#import "DatePickerView.h"
#import "FTAnimation+UIView.h"
#import "SettingViewController.h"


@interface TopicDetailsViewController ()<DatePickerViewDelegate ,UIGestureRecognizerDelegate >
{
    DatePickerView * pickerView;
    float currentFontSize;
    float mCurrentScale;
    float mLastScale;
}
@end

@implementation TopicDetailsViewController
@synthesize currentTopic;
@synthesize delegate;
@synthesize isFromFav;

-(id)initWithTopic:(TopicModel *)topic topicNumber:(int)number of:(int)count{
    NSString* nibNameOrNil=DEVICE_IS_PHONE?@"TopicDetailsViewController":@"TopicDetailsViewController_iPad";
    self=[super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
      //  NSLog(@"number : %i", number);
        self.currentTopic =topic;
        topicCountStr=[ NSString stringWithFormat:@"%i",number+1];
        //NSLog(topicCountStr);
        totalCountStr =[NSString stringWithFormat:@"/ %i",count];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.headerViewImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_header",currentTopic.categoryOrder]]];// currentTopic.categoryOrder
    [self.topicTextView setText:currentTopic.topicTitle];
    [self.pagetitle setText:currentTopic.categoryName];
    [self.favButton setSelected:currentTopic.isFav];
    [self.alarmButton setSelected:currentTopic.haveAlarm];
        UIPinchGestureRecognizer* zoomGesture=[[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(zoomInOutGestureAction:)];
    [zoomGesture setDelegate:self];
    [topicCount setText:topicCountStr];
    [totalCount setText:totalCountStr];
    [self.topicTextView addGestureRecognizer:zoomGesture];
    
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(orientationDidCahnged:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
   
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self orientationDidCahnged:nil];
}

-(void)viewWillAppear:(BOOL)animated{
      [super viewWillAppear:animated];
    int settingFont=[[[NSUserDefaults standardUserDefaults]objectForKey:@"setting"]integerValue];
   // NSLog(@"setting font : %i ", settingFont);
    currentFontSize=settingFont>0 ?settingFont: DEVICE_IS_PHONE?16:22 ;
   // NSLog(@"current font : %f ", currentFontSize);
    [self.topicTextView setFont:[UIFont boldSystemFontOfSize:currentFontSize]];
    if (isFromFav) {
        [self.headerIndexButton setHidden:YES];
        [self.headerHomeButton setSelected:NO];
    }else{
        [self.headerHomeButton setSelected:YES];
    }
    
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)orientationDidCahnged:(NSNotification*)object{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (DEVICE_IS_PHONE) {
        if (orientation==UIInterfaceOrientationPortrait) {
            [self.internalFooterBG setImage:[UIImage imageNamed:@"internal_footer_bg"]];
            
            [self.favButton setFrame:CGRectMake(183, self.favButton.frame.origin.y, self.favButton.frame.size.width, self.favButton.frame.size.height)];
            [self.alarmButton setFrame:CGRectMake(118, self.alarmButton.frame.origin.y, self.alarmButton.frame.size.width, self.alarmButton.frame.size.height)];
        }else{
            [self.internalFooterBG setImage:[UIImage imageNamed:@"internal_footer_bg_lndscp"]];
            [self.favButton setFrame:CGRectMake((DEVICE_IS_IPHONE5? 414 : 345)  , self.favButton.frame.origin.y, self.favButton.frame.size.width, self.favButton.frame.size.height)];
            [self.alarmButton setFrame:CGRectMake((DEVICE_IS_IPHONE5? 335: 281), self.alarmButton.frame.origin.y, self.alarmButton.frame.size.width, self.alarmButton.frame.size.height)];
        }
    }else{
        if (orientation==UIInterfaceOrientationPortrait || orientation ==UIInterfaceOrientationPortraitUpsideDown ) {
            [self.favButton setFrame:CGRectMake(552, self.favButton.frame.origin.y, self.favButton.frame.size.width, self.favButton.frame.size.height)];
            [self.alarmButton setFrame:CGRectMake(440, self.alarmButton.frame.origin.y, self.alarmButton.frame.size.width, self.alarmButton.frame.size.height)];
        }else{
            [self.favButton setFrame:CGRectMake(748 , self.favButton.frame.origin.y, self.favButton.frame.size.width, self.favButton.frame.size.height)];
            [self.alarmButton setFrame:CGRectMake(604, self.alarmButton.frame.origin.y, self.alarmButton.frame.size.width, self.alarmButton.frame.size.height)];
        }
    }
    /*
    if (orientation == UIInterfaceOrientationPortrait) {
      if(DEVICE_IS_PHONE)
          [self.internalFooterBG setImage:[UIImage imageNamed:@"internal_footer_bg"]];
        
        [self.favButton setFrame:CGRectMake(DEVICE_IS_PHONE? 183:552, self.favButton.frame.origin.y, self.favButton.frame.size.width, self.favButton.frame.size.height)];
        [self.alarmButton setFrame:CGRectMake(DEVICE_IS_PHONE? 118:440, self.alarmButton.frame.origin.y, self.alarmButton.frame.size.width, self.alarmButton.frame.size.height)];
        
    }else{
        [self.internalFooterBG setImage:[UIImage imageNamed:@"internal_footer_bg_lndscp"]];
        [self.favButton setFrame:CGRectMake(DEVICE_IS_PHONE? (DEVICE_IS_IPHONE5? 414 : 345) :748 , self.favButton.frame.origin.y, self.favButton.frame.size.width, self.favButton.frame.size.height)];
        [self.alarmButton setFrame:CGRectMake(DEVICE_IS_PHONE?(DEVICE_IS_IPHONE5? 335: 281):604, self.alarmButton.frame.origin.y, self.alarmButton.frame.size.width, self.alarmButton.frame.size.height)];
    }*/
}



#pragma -mark helper functions 
-(void)CheckAndAdjustNightMode{
    if(nightMode){
        [self.view setBackgroundColor:[UIColor blackColor]];
        [self.topicTextView setTextColor:[UIColor whiteColor]];
    }else{
        [self.view setBackgroundColor:[UIColor whiteColor]];
        [self.topicTextView setTextColor:[UIColor blackColor]];
    }
}

#pragma -mark IBActions
-(void)headerIndexButtonTouched:(UIButton *)sender{
    if ([delegate respondsToSelector:@selector(TopicViewDetailsViewControllerDidRequestShowIndex)]) {
        [delegate TopicViewDetailsViewControllerDidRequestShowIndex];
    }
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)settingButtonTouched:(UIButton *)sender {
    [self presentModalViewController:[SettingViewController new ] animated:YES];
}

- (IBAction)favoriteButtonTouched:(id)sender {
    [self.favButton setSelected:!currentTopic.isFav];
    [[appDataManger SharedInstence]AddRemoveTopicToFavorites:currentTopic];
    
}

- (IBAction)alarmButtounTouched:(UIButton *)sender {
    // alaram
     if (!pickerView) {
         NSString *nibname=[DatePickerView getNibName];
        // UIInterfaceOrientation ori=[[UIApplication sharedApplication]statusBarOrientation];
     pickerView=[[NSBundle mainBundle]loadNibNamed: nibname owner:self options:nil][0];
    [self.view addSubview:pickerView];
    [pickerView setDelegate:self];
    [pickerView showForTopic:currentTopic];

    }
}

- (IBAction)shareButtonTouched:(UIButton *)sender {
    [[appDataManger SharedInstence]shareTopic:currentTopic inViewController:self];
}


-(void)zoomInOutGestureAction:(UIPinchGestureRecognizer*)sender
{
    mCurrentScale = [sender scale];//- mLastScale;
    // mLastScale = [sender scale];
    NSLog(@"first result for current scale : %f for snder scale: %f",mCurrentScale,[sender scale]);
    /*if ([sender scale]<1.0) {
        mCurrentScale =mCurrentScale*-10;
    }
    
    if (! mLastScale ) {
        mCurrentScale=1;
    }
    NSLog(@"second result for current scale : %f",mCurrentScale);
    
   
    */
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        mLastScale = 1.0;
    }
    
  
 /*   currentFontSize=currentFontSize + mCurrentScale;
    */
    
    if (mCurrentScale>=1.0 && currentFontSize <= (DEVICE_IS_PHONE ?50:70)) {
        currentFontSize=currentFontSize+mCurrentScale;
    }else if(mCurrentScale <1.0 && currentFontSize > (DEVICE_IS_PHONE? 5:10)) {
        currentFontSize=currentFontSize-((0.9-mCurrentScale)*10);
    }
    [self.topicTextView setFont:[UIFont boldSystemFontOfSize:currentFontSize]];
    NSLog(@"Final Results  ..... \n gesture scale : %f  \n , current scale : %f \n latscale = %f  ,\n current font : %f  , \n -------------------------------------------------------------------",sender.scale , mCurrentScale , mLastScale , currentFontSize);
    
}



- (IBAction)zoomInButtonTouched:(UIButton *)sender {
        currentFontSize++;
        [self.topicTextView setFont:[UIFont boldSystemFontOfSize:currentFontSize]];
    if (currentFontSize >= (DEVICE_IS_PHONE? 35:45)) {
            [self.zoomInButton setEnabled:NO];
        }
    [self.zoomOutButton setEnabled:YES];
NSLog(@"zomming In , current font size : %f , current zoominButton state : %@ , current zoomOut button state : %@",currentFontSize,self.zoomInButton.enabled?@"enabled":@"disabled",self.zoomOutButton.enabled?@"enabled":@"disabled");
}

- (IBAction)zoomOutButtonTouched:(UIButton *)sender {
    currentFontSize --;
    [self.topicTextView setFont:[UIFont boldSystemFontOfSize:currentFontSize]];
    if (currentFontSize <= (DEVICE_IS_PHONE? 10:20)) {
        [self.zoomOutButton setEnabled:NO];
    }
    [self.zoomInButton setEnabled:YES];
    NSLog(@"zomming out , current font size : %f , current zoominButton state : %@ , current zoomOut button state : %@",currentFontSize,self.zoomInButton.enabled?@"enabled":@"disabled",self.zoomOutButton.enabled?@"enabled":@"disabled");
}

- (void)viewDidUnload {
    [self setTopicTextView:nil];
    [self setPagetitle:nil];
    [self setFavButton:nil];
    [self setAlarmButton:nil];
    [self setZoomInButton:nil];
    [self setZoomOutButton:nil];
    topicCount = nil;
    totalCount = nil;
    [self setInternalFooterBG:nil];
    [super viewDidUnload];
}



#pragma -mark datepicker delegate

-(void)DatePickerNewAlarmSettingFinished:(BOOL)succeeded{
    if (succeeded) {
        [self.alarmButton setSelected:YES];
    }
    pickerView=nil;
}




@end
