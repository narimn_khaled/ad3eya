//
//  TopicDetailsViewController.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/24/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopicModel.h"
#import "masterViewController.h"

@class TopicDetailsViewController;

@protocol TopicDetailsViewControllerDelegate <NSObject>

-(void)TopicViewDetailsViewControllerDidRequestShowIndex;

@end

@interface TopicDetailsViewController : masterViewController
{
    __weak IBOutlet UILabel *totalCount;
    __weak IBOutlet UILabel *topicCount;
    NSString *totalCountStr;
    NSString* topicCountStr;
}

@property (nonatomic) BOOL isFromFav;

@property (nonatomic ,weak) id<TopicDetailsViewControllerDelegate> delegate;

@property (nonatomic , retain ) TopicModel * currentTopic;
@property (weak, nonatomic) IBOutlet UITextView *topicTextView;
@property (weak, nonatomic) IBOutlet UILabel *pagetitle;

@property (weak, nonatomic) IBOutlet UIImageView *internalFooterBG;

@property (weak, nonatomic) IBOutlet UIButton *favButton;
@property (weak, nonatomic) IBOutlet UIButton *alarmButton;

@property (weak, nonatomic) IBOutlet UIButton *zoomInButton;
@property (weak, nonatomic) IBOutlet UIButton *zoomOutButton;


- (IBAction)favoriteButtonTouched:(UIButton*)sender;
- (IBAction)alarmButtounTouched:(UIButton *)sender;
- (IBAction)shareButtonTouched:(UIButton *)sender;

- (IBAction)zoomInButtonTouched:(UIButton *)sender;
- (IBAction)zoomOutButtonTouched:(UIButton *)sender;


-(id)initWithTopic:(TopicModel*)topic topicNumber:(int)number of:(int)count;
@end
