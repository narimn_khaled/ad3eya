//
//  TopicModel.m
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/25/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "TopicModel.h"

@implementation TopicModel
@synthesize topiccID=_topicID;
@synthesize topicTitle=_topicTitle;
@synthesize topicOrder=_topicOrder;
@synthesize categoryID=_categoryID;
@synthesize categoryName=_categoryName;
@synthesize isFav=_isFav;
@synthesize haveAlarm=_haveAlarm;
@synthesize alarmDate=_alarmDate;
@synthesize alarmNotification=_alarmNotification;
@synthesize categoryOrder=_categoryOrder;

- (id)init
{
    self = [super init];
    if (self) {
        self.topiccID=@"";
        self.topicTitle =@"title";
        self.categoryID=@"";
        self.categoryName=@"";
        self.categoryOrder=@"";
        self.isFav =NO;
        self.haveAlarm=NO;
        self.alarmDate=nil;
        self.alarmNotification=nil;
    }
    return self;
}


-(TopicModel *)initWithTitle:(NSString *)title{
    if ((self =[super init])) {
        self.topiccID=@"";
        self.topicTitle =title;
        self.categoryID=@"";
        self.categoryName=@"";
        self.categoryOrder=@"";
        self.isFav =NO;
        self.haveAlarm=NO;
        self.alarmDate=nil;
        self.alarmNotification=nil;
    }
    return self;
}

-(TopicModel *)initWithTitle:(NSString *)title category:(NSString *)cat andFav:(bool)fav{
    if ((self=[super init])) {
        self.topiccID=@"";
        self.topicTitle=title;
        self.categoryID=@"";
        self.categoryName=cat;
        self.categoryOrder=@"";
        self.isFav =fav;
        self.haveAlarm=NO;
        self.alarmDate=nil;
        self.alarmNotification=nil;
    }
    return self;
}


-(TopicModel *)initWithDictionary:(NSDictionary *)Dic{
    if ((self=[super init])) {
        self.topiccID=[Dic objectForKey:@"topicID"];
        self.topicTitle =[Dic objectForKey:@"topicTitle"];
        self.topicOrder=[[NSString stringWithFormat:@"%@",[Dic objectForKey:@"OrderT"]] integerValue];
        self.categoryID=[Dic objectForKey:@"categoryID"];
        self.categoryName=[Dic objectForKey:@"categoryName"];
        self.categoryOrder=[Dic objectForKey:@"Order"];
        self.haveAlarm=NO;
        self.isFav=NO;
        self.alarmDate=nil;
        self.alarmNotification=nil;
    }
    return self;
}

@end
