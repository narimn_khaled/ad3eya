//
//  TopicModel.h
//  Ad3eyaApp
//
//  Created by Ali Amin on 8/25/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TopicModel : NSObject
{
    NSString *_topicID;
    NSString *_topicTitle;
    int _topicOrder;
    NSString * _categoryID;
    NSString * _categoryName;
    NSString * _categoryOrder;
    BOOL _isFav;
    BOOL _haveAlarm;
    NSDate *_alarmDate;
    UILocalNotification * _alarmNotification;
}

@property (nonatomic , copy) NSString * topiccID;
@property (nonatomic , copy) NSString * topicTitle;
@property (nonatomic ) int  topicOrder;
@property (nonatomic , copy) NSString *categoryID;
@property (nonatomic , copy) NSString * categoryName;
@property (nonatomic , copy) NSString * categoryOrder;
@property (nonatomic ) BOOL isFav;
@property (nonatomic ) BOOL haveAlarm;
@property (nonatomic , retain)  NSDate * alarmDate;
@property (nonatomic , retain) UILocalNotification * alarmNotification;

-(TopicModel * )initWithTitle:(NSString*)title;
-(TopicModel *)initWithTitle:(NSString *)title category:(NSString*)cat andFav:(bool)fav;
-(TopicModel *)initWithDictionary:(NSDictionary*)Dic;


@end
